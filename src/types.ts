export enum HttpMethod {
  Post = 'post',
  Get = 'get',
  Delete = 'delete',
  Patch = 'patch',
  Put = 'put'
}

export namespace ErrorType {
  export enum InternalErrorType {
    Unknown = 'InternalUnknown',
    Uninitialized = 'InternalUninitialized',
    AuthMissing = 'InternalAuthMissing',
    BadData = 'InternalBadData',
    MissingParameter = 'InternalMissingParameter'
  }

  export type Type =
    | InternalErrorType
    | AuthErrorType
    | FileErrorType
    | ServerErrorType
    | GeneralErrorType
    | MessageErrorType
    | UserErrorType
    | ConversationErrorType

  export enum AuthErrorType {
    InvalidKeyErrorType = 'AuthErrorTypeInvalidKey',
    InvalidUserType = 'AuthErrorTypeInvalidUserType',
    NotFoundErrorType = 'AuthErrorTypeNotFound',
    MissingParamsErrorType = 'AuthErrorTypeMissingParams',
    InvalidPassword = 'AuthErrorTypeInvalidPassword',
    TokenExpiredType = 'AuthErrorTypeTokenExpired',
    AppHasNoNotificationSetup = 'AuthErrorTypeAppHasNoNotificationSetup',
    NotPartOfApp = 'AuthErrorTypeNotPartOfApp',
    InvalidWebhookKey = 'AuthErrorTypeInvalidWebhookKey',
    NoAccessToUser = 'AuthErrorTypeNoAccessToUser',
    ServerNoAccessToUser = 'AuthErrorTypeServerNoAccessToUser',
    IncorrectPassword = 'AuthErrorTypeIncorrectPassword',
    NoSamePassword = 'AuthErrorTypeNoSamePassword',
    NoAppAccessToUser = 'AuthErrorTypeNoAppAccessToUser',
    TokenNotStarted = 'AuthErrorTypeTokenNotStarted',
    AccountAlreadyExists = 'AuthErrorTypeAccountAlreadyExists',
    UserNotFound = 'AuthErrorTypeUserNotFound',
    InvalidToken = 'AuthErrorTypeInvalidToken',
    MissingAuthHeaderField = 'AuthErrorTypeMissingAuthHeaderField',
    InvalidCredentials = 'AuthErrorTypeInvalidCredentials',
    HashGeneration = 'AuthErrorTypeHashGeneration',
    HashComparison = 'AuthErrorTypeHashComparison',
    RefreshTokenAlreadyExchanged = 'AuthErrorTypeRefreshTokenAlreadyExchanged',
    RefreshTokenNotFound = 'AuthErrorTypeRefreshTokenNotFound',
    UserDisabled = 'AuthErrorTypeUserDisabled'
  }
  export enum FileErrorType {
    SizeLimitExceeded = 'FileErrorTypeSizeLimitExceeded',
    NoFileSentType = 'FileErrorTypeNoFileSentType',
    InvalidFieldNameForFile = 'FileErrorTypeInvalidFieldNameForFile',
    InvalidMetadata = 'FileErrorTypeInvalidMetadata',
    FileSizeLimitExceeded = 'FileErrorTypeFileSizeLimitExceeded',
    InvalidFileName = 'FileErrorTypeInvalidFileName',
    NotSent = 'FileErrorTypeNotSent',
    NotFound = 'FileErrorTypeNotFound',
    NoThumbnailFound = 'FileErrorTypeNoThumbnailFound',
    ImageNotForConversation = 'FileErrorTypeImageNotForConversation',
    ImageNotForUser = 'FileErrorTypeImageNotForUser',
    FileNotForMessage = 'FileErrorTypeFileNotForMessage',
    SizeNotFound = 'FileErrorTypeSizeNotFound',
    NotForApp = 'FileErrorTypeNotForApp',
    IncorrectFileType = 'FileErrorTypeIncorrectFileType'
  }
  export enum ServerErrorType {
    UnknownError = 'UnknownError',
    InternalServerError = 'InternalServerError'
  }
  export enum GeneralErrorType {
    MissingQueryParams = 'GeneralErrorTypeMissingQueryParams',
    RouteNotFound = 'GeneralErrorTypeRouteNotFound',
    LimitNotValid = 'GeneralErrorTypeLimitNotValid',
    OffsetNotValid = 'GeneralErrorTypeOffsetNotValid'
  }
  export enum MessageErrorType {
    InvalidDate = 'MessageErrorTypeInvalidDate',
    NotFound = 'MessageErrorTypeNotFound',
    NotFoundForUser = 'MessageErrorTypeNotFoundForUser',
    MessageAlreadyRead = 'MessageErrorTypeMessageAlreadyRead',
    CannotDeleteSystemMessage = 'MessageErrorTypeCannotDeleteSystemMessage'
  }
  export enum DateErrorType {
    InvalidFromDateError = 'DateErrorTypeInvalidFromDateError',
    InvalidToDateError = 'DateErrorTypeInvalidToDateError'
  }
  export enum UserErrorType {
    AlreadyMuted = 'UserErrorTypeAlreadyMuted',
    NotOnMute = 'UserErrorTypeNotOnMute',
    AlreadyEnabled = 'UserErrorTypeAlreadyEnabled',
    AlreadyDisabled = 'UserErrorTypeAlreadyDisabled'
  }
  export enum ConversationErrorType {
    NotFound = 'ConversationErrorTypeNotFound',
    CannotConverseWithUser = 'ConversationErrorTypeCannotConverseWithUser',
    NotAnOwner = 'ConversationErrorTypeNotAnOwner',
    AlreadyAnOwner = 'ConversationErrorTypeAlreadyAnOwner',
    NotFoundForApp = 'ConversationErrorTypeNotFoundForApp',
    InvalidType = 'ConversationErrorTypeInvalidType',
    InvalidConversationUserType = 'ConversationErrorTypeInvalidConversationUserType',
    MustBeOwner = 'ConversationErrorTypeMustBeOwner',
    NotPartOfApp = 'ConversationErrorTypeNotPartOfApp',
    NotFoundForUser = 'ConversationErrorTypeNotFoundForUser',
    AlreadyExists = 'ConversationErrorTypeAlreadyExists',
    CannotChatWithSelf = 'ConversationErrorTypeCannotChatWithSelf',
    AlreadyMuted = 'ConversationErrorTypeAlreadyMuted',
    NotOnMute = 'ConversationErrorTypeNotOnMute',
    AlreadyPinned = 'ConversationErrorTypeAlreadyPinned',
    NotPinned = 'ConversationErrorTypeNotPinned',
    UsersNotInApp = 'ConversationErrorTypeUsersNotInApp',
    NoValidUsersToAdd = 'ConversationErrorTypeNoValidUsersToAdd',
    NoValidUsersToRemove = 'ConversationErrorTypeNoValidUsersToRemove',
    CannotChangeAccessToSelf = 'ConversationErrorTypeCannotChangeAccessToSelf',
    CannotAddExistingUsers = 'ConversationErrorTypeCannotAddExistingUsers',
    AlreadyRemovedUsers = 'ConversationErrorTypeAlreadyRemovedUsers',
    UsersNotInConversation = 'ConversationErrorTypeUsersNotInConversation',
    CannotRemoveOwners = 'ConversationErrorTypeCannotRemoveOwners'
  }
}

export class AppError {
  type: ErrorType.Type = ErrorType.InternalErrorType.Unknown

  statusCode?: number

  error?: Error

  message?: string

  authFailed?: boolean

  exchange?: boolean

  setErrorActions = (params: { authFailed?: boolean; exchange?: boolean }) => {
    const { authFailed, exchange } = params
    this.exchange = exchange
    this.authFailed = authFailed
  }

  constructor(err?: string | Error, type?: ErrorType.Type) {
    if (type) {
      this.type = type
    }
    if (err instanceof Error) {
      this.error = err
      this.message = err.message
    } else {
      this.message = err
    }
  }

  setStatusCode = (statusCode?: number) => {
    this.statusCode = statusCode
  }

  setMessage = (msg: string) => {
    this.message = msg
  }
}

export class DefaultGroupProfileImageBase {
  filename!: string

  original!: string

  thumbnail!: string
}

export class AuthBase {
  accessToken!: string

  publisherToken!: string

  refreshToken!: string
}

class UserBase {
  id!: string

  firstName!: string

  lastName!: string

  refId!: string | null
}

class UserDetail extends UserBase {
  email!: string
}

export enum FileType {
  Video = 'video',
  Image = 'image',
  Audio = 'audio',
  Other = 'other'
}

export enum ImageSizes {
  Tiny = 'tiny',
  Small = 'small',
  Medium = 'medium',
  Large = 'large',
  XLarge = 'xlarge'
}

export class SubImageMinimal {
  imageSize!: ImageSizes

  width!: number

  height!: number

  postfix!: string
}

export class ImageInfoMedia {
  width!: number

  height!: number

  subImagesInfo!: SubImageMinimal[]
}

class Paginatable {
  search!: string | null

  totalCount!: number

  offset!: number

  nextOffset!: number

  limit!: number

  completed!: boolean
}

class ConversationConversableUsersBatchMeta extends Paginatable {
  includeRefIdInSearch!: boolean

  conversationId!: string
}

class ConversationUsersBatchMeta extends Paginatable {
  includeRefIdInSearch!: boolean

  conversationId!: string
}

enum ConversationUserQueryType {
  All = 'all',
  Current = 'current',
  Removed = 'removed'
}

export class ConversationUsersBatchBase extends ConversationUsersBatchMeta {
  type!: ConversationUserQueryType
}

export class MediaBase {
  urlPath!: string

  mimeType!: string

  extension!: string

  encoding!: string

  filename!: string

  fileSize!: number
}

export class ImageBase extends MediaBase {
  fileType = FileType.Image

  imageInfo!: ImageInfoMedia

  videoInfo = null
}

export class VideoBase extends MediaBase {
  fileType = FileType.Video

  imageInfo = null

  videoInfo!: {
    duration: number
    thumbnail: ImageBase | null
  }
}

export class AudioBase extends MediaBase {
  fileType = FileType.Audio

  imageInfo = null

  videoInfo!: {
    duration: number
    thumbnail: null
  }
}

export class DocumentBase extends MediaBase {
  fileType = FileType.Other

  imageInfo = null

  videoInfo = null
}

export type FileBase = ImageBase | VideoBase | AudioBase | DocumentBase

export enum SystemMessageType {
  MemberRemoved = 'MemberRemoved',
  MemberAdded = 'MemberAdded',
  OwnerRemoved = 'OwnerRemoved',
  OwnerAdded = 'OwnerAdded',
  GroupPictureChanged = 'GroupPictureChanged',
  GroupNameChanged = 'GroupNameChanged',
  GroupDisabled = 'GroupDisabled',
  GroupCreated = 'GroupCreated',
  IndividualChatCreated = 'IndividualChatCreated'
}

export class UserSimpleInfo {
  firstName!: string

  lastName!: string

  id!: string

  refId!: string | null
}

export class SystemMessageBase {
  systemMessageType!: SystemMessageType

  id!: string

  refValue!: string | null

  actorCount!: number

  initiator!: UserSimpleInfo | null

  actors!: UserSimpleInfo[]

  text?: string | null
}

export enum MessageType {
  System = 'system',
  Text = 'text',
  Image = 'image',
  Video = 'video',
  Document = 'document',
  Gif = 'gif',
  Emoji = 'emoji',
  Audio = 'audio'
}

export enum ReactionType {
  Thumb = 'Thumb',
  Heart = 'Heart',
  Clap = 'Clap',
  HaHa = 'HaHa',
  Exclamation = 'Exclamation'
}
export class ReactionsObject {
  [ReactionType.Thumb] = 0;

  [ReactionType.Heart] = 0;

  [ReactionType.Clap] = 0;

  [ReactionType.HaHa] = 0;

  [ReactionType.Exclamation] = 0
}

export class MessageBase {
  id!: string

  conversationId!: string

  type!: MessageType

  text!: string | null

  refUrl!: string | null

  sequence!: number

  reactions!: ReactionsObject
}

export enum ConversationType {
  Group = 'group',
  Individual = 'individual'
}

export class ConversationBase {
  id!: string

  lastReadMessageId!: string | null

  type!: ConversationType.Group | ConversationType.Individual

  isOwner!: boolean

  visible!: boolean

  serverCreated!: boolean

  serverManaged!: boolean
}

export namespace DTO {
  export class DefaultGroupProfileImage extends DefaultGroupProfileImageBase {}
  export class Auth extends AuthBase {}
  export type Media = FileBase
  export class User extends UserDetail {
    picture!: Media | null

    disabled!: boolean

    deletedAt!: string | null
  }
  export class UserSelf extends User {
    muteAllUntil!: string | null
  }
  export class ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {
    results!: User[]
  }
  export class ConversationUser extends User {
    isOwner!: boolean

    deletedFromConversationAt!: string | null
  }
  export class ConversationUsersBatch extends ConversationUsersBatchBase {
    results!: ConversationUser[]
  }
  export class SystemMessage extends SystemMessageBase {}
  export class Message extends MessageBase {
    createdAt!: string

    sender!: User | null

    file!: Media | null

    sent!: boolean

    systemMessage!: SystemMessage | null

    deletedAt!: string | null
  }
  export class ConversationInfo {
    picture!: Media | null

    name!: string
  }
  export class GroupConversationInfo extends ConversationInfo {}
  export class UserConversationInfo extends ConversationInfo {
    id!: string

    email!: string

    refId!: string | null
  }
  export class Conversation extends ConversationBase {
    muteUntil!: string | null

    pinnedAt!: string | null

    badge!: number | null

    startAfter!: string | null

    endBefore!: string | null

    deletedFromConversationAt!: string | null

    createdAt!: string

    updatedAt!: string

    lastMessage!: Message | null

    group?: ConversationInfo

    user?: UserConversationInfo
  }
  export class GroupConversation extends Conversation {
    type!: ConversationType.Group

    group!: GroupConversationInfo
  }
  export class IndividualConversation extends Conversation {
    type!: ConversationType.Individual

    user!: UserConversationInfo
  }
  export type GenericConversation = GroupConversation | IndividualConversation
  export class MessageDeleted {
    conversationId!: string

    messageId!: string

    deletedAt!: string
  }
  export class ConversationUsersBasic {
    isOwner!: boolean

    id!: string

    deletedFromConversationAt!: string | null

    userDisabled!: boolean

    userDeletedAt!: string | null
  }
}

export namespace Domain {
  export class DefaultGroupProfileImage extends DefaultGroupProfileImageBase {}
  export class Auth extends AuthBase {}
  export type Media = MediaBase
  export class User extends UserDetail {
    picture!: Media | null

    disabled!: boolean

    deletedAt!: Date | null
  }
  export class UserSelf extends User {
    muteAllUntil!: Date | null
  }
  export class ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {
    results!: User[]
  }
  export class ConversationUser extends User {
    deletedFromConversationAt!: Date | null

    isOwner!: boolean
  }
  export class ConversationUsersBatch extends ConversationUsersBatchBase {
    results!: ConversationUser[]
  }
  export class SystemMessage extends SystemMessageBase {}
  export class Message extends MessageBase {
    createdAt!: Date

    sender!: User | null

    file!: Media | null

    sent!: boolean

    systemMessage!: SystemMessage | null

    deletedAt!: Date | null
  }
  export class ConversationInfo {
    picture!: Media | null

    name!: string
  }
  export class GroupConversationInfo extends ConversationInfo {}
  export class UserConversationInfo extends ConversationInfo {
    id!: string

    email!: string

    refId!: string | null
  }
  export class Conversation extends ConversationBase {
    muteUntil!: Date | null

    pinnedAt!: Date | null

    badge!: number | null

    startAfter!: Date | null

    endBefore!: Date | null

    deletedFromConversationAt!: Date | null

    createdAt!: Date

    updatedAt!: Date

    lastMessage!: Message | null
  }
  export class GroupConversation extends Conversation {
    type!: ConversationType.Group

    group!: GroupConversationInfo
  }
  export class IndividualConversation extends Conversation {
    type!: ConversationType.Individual

    user!: UserConversationInfo
  }
  export type GenericConversation = GroupConversation | IndividualConversation
  export class MessageDeleted {
    conversationId!: string

    messageId!: string

    deletedAt!: Date
  }
  export class ConversationUsersBasic {
    isOwner!: boolean

    id!: string

    deletedFromConversationAt!: Date | null

    userDisabled!: boolean

    userDeletedAt!: Date | null
  }
}
