[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [FileType](filetype.md)

# Enumeration: FileType

## Index

### Enumeration members

- [Audio](filetype.md#markdown-header-audio)
- [Image](filetype.md#markdown-header-image)
- [Other](filetype.md#markdown-header-other)
- [Video](filetype.md#markdown-header-video)

## Enumeration members

### Audio

• **Audio**: = "audio"

Defined in types.ts:200

---

### Image

• **Image**: = "image"

Defined in types.ts:199

---

### Other

• **Other**: = "other"

Defined in types.ts:201

---

### Video

• **Video**: = "video"

Defined in types.ts:198
