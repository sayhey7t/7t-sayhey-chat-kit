[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [GeneralErrorType](errortype.generalerrortype.md)

# Enumeration: GeneralErrorType

## Index

### Enumeration members

* [LimitNotValid](errortype.generalerrortype.md#markdown-header-limitnotvalid)
* [MissingQueryParams](errortype.generalerrortype.md#markdown-header-missingqueryparams)
* [OffsetNotValid](errortype.generalerrortype.md#markdown-header-offsetnotvalid)
* [RouteNotFound](errortype.generalerrortype.md#markdown-header-routenotfound)

## Enumeration members

###  LimitNotValid

• **LimitNotValid**: = "GeneralErrorTypeLimitNotValid"

Defined in types.ts:79

___

###  MissingQueryParams

• **MissingQueryParams**: = "GeneralErrorTypeMissingQueryParams"

Defined in types.ts:77

___

###  OffsetNotValid

• **OffsetNotValid**: = "GeneralErrorTypeOffsetNotValid"

Defined in types.ts:80

___

###  RouteNotFound

• **RouteNotFound**: = "GeneralErrorTypeRouteNotFound"

Defined in types.ts:78
