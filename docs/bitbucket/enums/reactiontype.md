[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ReactionType](reactiontype.md)

# Enumeration: ReactionType

## Index

### Enumeration members

- [Clap](reactiontype.md#markdown-header-clap)
- [Exclamation](reactiontype.md#markdown-header-exclamation)
- [HaHa](reactiontype.md#markdown-header-haha)
- [Heart](reactiontype.md#markdown-header-heart)
- [Thumb](reactiontype.md#markdown-header-thumb)

## Enumeration members

### Clap

• **Clap**: = "Clap"

Defined in types.ts:372

---

### Exclamation

• **Exclamation**: = "Exclamation"

Defined in types.ts:374

---

### HaHa

• **HaHa**: = "HaHa"

Defined in types.ts:373

---

### Heart

• **Heart**: = "Heart"

Defined in types.ts:371

---

### Thumb

• **Thumb**: = "Thumb"

Defined in types.ts:370
