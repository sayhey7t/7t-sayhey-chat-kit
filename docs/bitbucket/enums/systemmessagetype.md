[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [SystemMessageType](systemmessagetype.md)

# Enumeration: SystemMessageType

## Index

### Enumeration members

- [GroupCreated](systemmessagetype.md#markdown-header-groupcreated)
- [GroupDisabled](systemmessagetype.md#markdown-header-groupdisabled)
- [GroupNameChanged](systemmessagetype.md#markdown-header-groupnamechanged)
- [GroupPictureChanged](systemmessagetype.md#markdown-header-grouppicturechanged)
- [IndividualChatCreated](systemmessagetype.md#markdown-header-individualchatcreated)
- [MemberAdded](systemmessagetype.md#markdown-header-memberadded)
- [MemberRemoved](systemmessagetype.md#markdown-header-memberremoved)
- [OwnerAdded](systemmessagetype.md#markdown-header-owneradded)
- [OwnerRemoved](systemmessagetype.md#markdown-header-ownerremoved)

## Enumeration members

### GroupCreated

• **GroupCreated**: = "GroupCreated"

Defined in types.ts:328

---

### GroupDisabled

• **GroupDisabled**: = "GroupDisabled"

Defined in types.ts:327

---

### GroupNameChanged

• **GroupNameChanged**: = "GroupNameChanged"

Defined in types.ts:326

---

### GroupPictureChanged

• **GroupPictureChanged**: = "GroupPictureChanged"

Defined in types.ts:325

---

### IndividualChatCreated

• **IndividualChatCreated**: = "IndividualChatCreated"

Defined in types.ts:329

---

### MemberAdded

• **MemberAdded**: = "MemberAdded"

Defined in types.ts:322

---

### MemberRemoved

• **MemberRemoved**: = "MemberRemoved"

Defined in types.ts:321

---

### OwnerAdded

• **OwnerAdded**: = "OwnerAdded"

Defined in types.ts:324

---

### OwnerRemoved

• **OwnerRemoved**: = "OwnerRemoved"

Defined in types.ts:323
