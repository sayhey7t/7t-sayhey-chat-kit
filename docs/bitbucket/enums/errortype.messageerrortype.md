[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [MessageErrorType](errortype.messageerrortype.md)

# Enumeration: MessageErrorType

## Index

### Enumeration members

* [CannotDeleteSystemMessage](errortype.messageerrortype.md#markdown-header-cannotdeletesystemmessage)
* [InvalidDate](errortype.messageerrortype.md#markdown-header-invaliddate)
* [MessageAlreadyRead](errortype.messageerrortype.md#markdown-header-messagealreadyread)
* [NotFound](errortype.messageerrortype.md#markdown-header-notfound)
* [NotFoundForUser](errortype.messageerrortype.md#markdown-header-notfoundforuser)

## Enumeration members

###  CannotDeleteSystemMessage

• **CannotDeleteSystemMessage**: = "MessageErrorTypeCannotDeleteSystemMessage"

Defined in types.ts:87

___

###  InvalidDate

• **InvalidDate**: = "MessageErrorTypeInvalidDate"

Defined in types.ts:83

___

###  MessageAlreadyRead

• **MessageAlreadyRead**: = "MessageErrorTypeMessageAlreadyRead"

Defined in types.ts:86

___

###  NotFound

• **NotFound**: = "MessageErrorTypeNotFound"

Defined in types.ts:84

___

###  NotFoundForUser

• **NotFoundForUser**: = "MessageErrorTypeNotFoundForUser"

Defined in types.ts:85
