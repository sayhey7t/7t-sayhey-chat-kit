[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ImageSizes](imagesizes.md)

# Enumeration: ImageSizes

## Index

### Enumeration members

- [Large](imagesizes.md#markdown-header-large)
- [Medium](imagesizes.md#markdown-header-medium)
- [Small](imagesizes.md#markdown-header-small)
- [Tiny](imagesizes.md#markdown-header-tiny)
- [XLarge](imagesizes.md#markdown-header-xlarge)

## Enumeration members

### Large

• **Large**: = "large"

Defined in types.ts:208

---

### Medium

• **Medium**: = "medium"

Defined in types.ts:207

---

### Small

• **Small**: = "small"

Defined in types.ts:206

---

### Tiny

• **Tiny**: = "tiny"

Defined in types.ts:205

---

### XLarge

• **XLarge**: = "xlarge"

Defined in types.ts:209
