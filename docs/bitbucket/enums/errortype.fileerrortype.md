[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [FileErrorType](errortype.fileerrortype.md)

# Enumeration: FileErrorType

## Index

### Enumeration members

* [FileNotForMessage](errortype.fileerrortype.md#markdown-header-filenotformessage)
* [FileSizeLimitExceeded](errortype.fileerrortype.md#markdown-header-filesizelimitexceeded)
* [ImageNotForConversation](errortype.fileerrortype.md#markdown-header-imagenotforconversation)
* [ImageNotForUser](errortype.fileerrortype.md#markdown-header-imagenotforuser)
* [IncorrectFileType](errortype.fileerrortype.md#markdown-header-incorrectfiletype)
* [InvalidFieldNameForFile](errortype.fileerrortype.md#markdown-header-invalidfieldnameforfile)
* [InvalidFileName](errortype.fileerrortype.md#markdown-header-invalidfilename)
* [InvalidMetadata](errortype.fileerrortype.md#markdown-header-invalidmetadata)
* [NoFileSentType](errortype.fileerrortype.md#markdown-header-nofilesenttype)
* [NoThumbnailFound](errortype.fileerrortype.md#markdown-header-nothumbnailfound)
* [NotForApp](errortype.fileerrortype.md#markdown-header-notforapp)
* [NotFound](errortype.fileerrortype.md#markdown-header-notfound)
* [NotSent](errortype.fileerrortype.md#markdown-header-notsent)
* [SizeLimitExceeded](errortype.fileerrortype.md#markdown-header-sizelimitexceeded)
* [SizeNotFound](errortype.fileerrortype.md#markdown-header-sizenotfound)

## Enumeration members

###  FileNotForMessage

• **FileNotForMessage**: = "FileErrorTypeFileNotForMessage"

Defined in types.ts:67

___

###  FileSizeLimitExceeded

• **FileSizeLimitExceeded**: = "FileErrorTypeFileSizeLimitExceeded"

Defined in types.ts:60

___

###  ImageNotForConversation

• **ImageNotForConversation**: = "FileErrorTypeImageNotForConversation"

Defined in types.ts:65

___

###  ImageNotForUser

• **ImageNotForUser**: = "FileErrorTypeImageNotForUser"

Defined in types.ts:66

___

###  IncorrectFileType

• **IncorrectFileType**: = "FileErrorTypeIncorrectFileType"

Defined in types.ts:70

___

###  InvalidFieldNameForFile

• **InvalidFieldNameForFile**: = "FileErrorTypeInvalidFieldNameForFile"

Defined in types.ts:58

___

###  InvalidFileName

• **InvalidFileName**: = "FileErrorTypeInvalidFileName"

Defined in types.ts:61

___

###  InvalidMetadata

• **InvalidMetadata**: = "FileErrorTypeInvalidMetadata"

Defined in types.ts:59

___

###  NoFileSentType

• **NoFileSentType**: = "FileErrorTypeNoFileSentType"

Defined in types.ts:57

___

###  NoThumbnailFound

• **NoThumbnailFound**: = "FileErrorTypeNoThumbnailFound"

Defined in types.ts:64

___

###  NotForApp

• **NotForApp**: = "FileErrorTypeNotForApp"

Defined in types.ts:69

___

###  NotFound

• **NotFound**: = "FileErrorTypeNotFound"

Defined in types.ts:63

___

###  NotSent

• **NotSent**: = "FileErrorTypeNotSent"

Defined in types.ts:62

___

###  SizeLimitExceeded

• **SizeLimitExceeded**: = "FileErrorTypeSizeLimitExceeded"

Defined in types.ts:56

___

###  SizeNotFound

• **SizeNotFound**: = "FileErrorTypeSizeNotFound"

Defined in types.ts:68
