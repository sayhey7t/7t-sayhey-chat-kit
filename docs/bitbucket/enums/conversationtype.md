[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationType](conversationtype.md)

# Enumeration: ConversationType

## Index

### Enumeration members

- [Group](conversationtype.md#markdown-header-group)
- [Individual](conversationtype.md#markdown-header-individual)

## Enumeration members

### Group

• **Group**: = "group"

Defined in types.ts:405

---

### Individual

• **Individual**: = "individual"

Defined in types.ts:406
