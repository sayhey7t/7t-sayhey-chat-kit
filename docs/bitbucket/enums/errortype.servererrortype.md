[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [ServerErrorType](errortype.servererrortype.md)

# Enumeration: ServerErrorType

## Index

### Enumeration members

* [InternalServerError](errortype.servererrortype.md#markdown-header-internalservererror)
* [UnknownError](errortype.servererrortype.md#markdown-header-unknownerror)

## Enumeration members

###  InternalServerError

• **InternalServerError**: = "InternalServerError"

Defined in types.ts:74

___

###  UnknownError

• **UnknownError**: = "UnknownError"

Defined in types.ts:73
