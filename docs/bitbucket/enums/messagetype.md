[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MessageType](messagetype.md)

# Enumeration: MessageType

## Index

### Enumeration members

- [Audio](messagetype.md#markdown-header-audio)
- [Document](messagetype.md#markdown-header-document)
- [Emoji](messagetype.md#markdown-header-emoji)
- [Gif](messagetype.md#markdown-header-gif)
- [Image](messagetype.md#markdown-header-image)
- [System](messagetype.md#markdown-header-system)
- [Text](messagetype.md#markdown-header-text)
- [Video](messagetype.md#markdown-header-video)

## Enumeration members

### Audio

• **Audio**: = "audio"

Defined in types.ts:366

---

### Document

• **Document**: = "document"

Defined in types.ts:363

---

### Emoji

• **Emoji**: = "emoji"

Defined in types.ts:365

---

### Gif

• **Gif**: = "gif"

Defined in types.ts:364

---

### Image

• **Image**: = "image"

Defined in types.ts:361

---

### System

• **System**: = "system"

Defined in types.ts:359

---

### Text

• **Text**: = "text"

Defined in types.ts:360

---

### Video

• **Video**: = "video"

Defined in types.ts:362
