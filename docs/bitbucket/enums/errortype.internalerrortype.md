[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [InternalErrorType](errortype.internalerrortype.md)

# Enumeration: InternalErrorType

## Index

### Enumeration members

* [AuthMissing](errortype.internalerrortype.md#markdown-header-authmissing)
* [BadData](errortype.internalerrortype.md#markdown-header-baddata)
* [MissingParameter](errortype.internalerrortype.md#markdown-header-missingparameter)
* [Uninitialized](errortype.internalerrortype.md#markdown-header-uninitialized)
* [Unknown](errortype.internalerrortype.md#markdown-header-unknown)

## Enumeration members

###  AuthMissing

• **AuthMissing**: = "InternalAuthMissing"

Defined in types.ts:13

___

###  BadData

• **BadData**: = "InternalBadData"

Defined in types.ts:14

___

###  MissingParameter

• **MissingParameter**: = "InternalMissingParameter"

Defined in types.ts:15

___

###  Uninitialized

• **Uninitialized**: = "InternalUninitialized"

Defined in types.ts:12

___

###  Unknown

• **Unknown**: = "InternalUnknown"

Defined in types.ts:11
