[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [Auth](dto.auth.md)

# Class: Auth

## Hierarchy

- [AuthBase](authbase.md)

  ↳ **Auth**

## Index

### Properties

- [accessToken](dto.auth.md#markdown-header-accesstoken)
- [publisherToken](dto.auth.md#markdown-header-publishertoken)
- [refreshToken](dto.auth.md#markdown-header-refreshtoken)

## Properties

### accessToken

• **accessToken**: _string_

_Inherited from [AuthBase](authbase.md).[accessToken](authbase.md#markdown-header-accesstoken)_

Defined in types.ts:176

---

### publisherToken

• **publisherToken**: _string_

_Inherited from [AuthBase](authbase.md).[publisherToken](authbase.md#markdown-header-publishertoken)_

Defined in types.ts:178

---

### refreshToken

• **refreshToken**: _string_

_Inherited from [AuthBase](authbase.md).[refreshToken](authbase.md#markdown-header-refreshtoken)_

Defined in types.ts:180
