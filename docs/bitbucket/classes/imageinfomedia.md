[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ImageInfoMedia](imageinfomedia.md)

# Class: ImageInfoMedia

## Hierarchy

- **ImageInfoMedia**

## Index

### Properties

- [height](imageinfomedia.md#markdown-header-height)
- [subImagesInfo](imageinfomedia.md#markdown-header-subimagesinfo)
- [width](imageinfomedia.md#markdown-header-width)

## Properties

### height

• **height**: _number_

Defined in types.ts:225

---

### subImagesInfo

• **subImagesInfo**: _[SubImageMinimal](subimageminimal.md)[]_

Defined in types.ts:227

---

### width

• **width**: _number_

Defined in types.ts:223
