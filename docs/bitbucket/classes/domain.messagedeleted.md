[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [MessageDeleted](domain.messagedeleted.md)

# Class: MessageDeleted

## Hierarchy

- **MessageDeleted**

## Index

### Properties

- [conversationId](domain.messagedeleted.md#markdown-header-conversationid)
- [deletedAt](domain.messagedeleted.md#markdown-header-deletedat)
- [messageId](domain.messagedeleted.md#markdown-header-messageid)

## Properties

### conversationId

• **conversationId**: _string_

Defined in types.ts:614

---

### deletedAt

• **deletedAt**: _Date_

Defined in types.ts:618

---

### messageId

• **messageId**: _string_

Defined in types.ts:616
