[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [IndividualConversation](domain.individualconversation.md)

# Class: IndividualConversation

## Hierarchy

↳ [Conversation](domain.conversation.md)

↳ **IndividualConversation**

## Index

### Properties

- [badge](domain.individualconversation.md#markdown-header-badge)
- [createdAt](domain.individualconversation.md#markdown-header-createdat)
- [deletedFromConversationAt](domain.individualconversation.md#markdown-header-deletedfromconversationat)
- [endBefore](domain.individualconversation.md#markdown-header-endbefore)
- [id](domain.individualconversation.md#markdown-header-id)
- [isOwner](domain.individualconversation.md#markdown-header-isowner)
- [lastMessage](domain.individualconversation.md#markdown-header-lastmessage)
- [lastReadMessageId](domain.individualconversation.md#markdown-header-lastreadmessageid)
- [muteUntil](domain.individualconversation.md#markdown-header-muteuntil)
- [pinnedAt](domain.individualconversation.md#markdown-header-pinnedat)
- [serverCreated](domain.individualconversation.md#markdown-header-servercreated)
- [serverManaged](domain.individualconversation.md#markdown-header-servermanaged)
- [startAfter](domain.individualconversation.md#markdown-header-startafter)
- [type](domain.individualconversation.md#markdown-header-type)
- [updatedAt](domain.individualconversation.md#markdown-header-updatedat)
- [user](domain.individualconversation.md#markdown-header-user)
- [visible](domain.individualconversation.md#markdown-header-visible)

## Properties

### badge

• **badge**: _number | null_

_Inherited from [Conversation](domain.conversation.md).[badge](domain.conversation.md#markdown-header-badge)_

Defined in types.ts:588

---

### createdAt

• **createdAt**: _Date_

_Inherited from [Conversation](domain.conversation.md).[createdAt](domain.conversation.md#markdown-header-createdat)_

Defined in types.ts:596

---

### deletedFromConversationAt

• **deletedFromConversationAt**: _Date | null_

_Inherited from [Conversation](domain.conversation.md).[deletedFromConversationAt](domain.conversation.md#markdown-header-deletedfromconversationat)_

Defined in types.ts:594

---

### endBefore

• **endBefore**: _Date | null_

_Inherited from [Conversation](domain.conversation.md).[endBefore](domain.conversation.md#markdown-header-endbefore)_

Defined in types.ts:592

---

### id

• **id**: _string_

_Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)_

Defined in types.ts:410

---

### isOwner

• **isOwner**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)_

Defined in types.ts:416

---

### lastMessage

• **lastMessage**: _[Message](domain.message.md) | null_

_Inherited from [Conversation](domain.conversation.md).[lastMessage](domain.conversation.md#markdown-header-lastmessage)_

Defined in types.ts:600

---

### lastReadMessageId

• **lastReadMessageId**: _string | null_

_Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)_

Defined in types.ts:412

---

### muteUntil

• **muteUntil**: _Date | null_

_Inherited from [Conversation](domain.conversation.md).[muteUntil](domain.conversation.md#markdown-header-muteuntil)_

Defined in types.ts:584

---

### pinnedAt

• **pinnedAt**: _Date | null_

_Inherited from [Conversation](domain.conversation.md).[pinnedAt](domain.conversation.md#markdown-header-pinnedat)_

Defined in types.ts:586

---

### serverCreated

• **serverCreated**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)_

Defined in types.ts:420

---

### serverManaged

• **serverManaged**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)_

Defined in types.ts:422

---

### startAfter

• **startAfter**: _Date | null_

_Inherited from [Conversation](domain.conversation.md).[startAfter](domain.conversation.md#markdown-header-startafter)_

Defined in types.ts:590

---

### type

• **type**: _[Individual](../enums/conversationtype.md#markdown-header-individual)_

_Overrides [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)_

Defined in types.ts:608

---

### updatedAt

• **updatedAt**: _Date_

_Inherited from [Conversation](domain.conversation.md).[updatedAt](domain.conversation.md#markdown-header-updatedat)_

Defined in types.ts:598

---

### user

• **user**: _[UserConversationInfo](domain.userconversationinfo.md)_

Defined in types.ts:610

---

### visible

• **visible**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)_

Defined in types.ts:418
