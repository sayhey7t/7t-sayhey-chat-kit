[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ReactionsObject](reactionsobject.md)

# Class: ReactionsObject

## Hierarchy

- **ReactionsObject**

## Index

### Properties

- [[ReactionType.Clap]](reactionsobject.md#markdown-header-[reactiontype.clap])
- [[ReactionType.Exclamation]](reactionsobject.md#markdown-header-[reactiontype.exclamation])
- [[ReactionType.HaHa]](reactionsobject.md#markdown-header-[reactiontype.haha])
- [[ReactionType.Heart]](reactionsobject.md#markdown-header-[reactiontype.heart])
- [[ReactionType.Thumb]](reactionsobject.md#markdown-header-[reactiontype.thumb])

## Properties

### [ReactionType.Clap]

• **[ReactionType.Clap]**: _number_ = 0

Defined in types.ts:381

---

### [ReactionType.Exclamation]

• **[ReactionType.Exclamation]**: _number_ = 0

Defined in types.ts:385

---

### [ReactionType.HaHa]

• **[ReactionType.HaHa]**: _number_ = 0

Defined in types.ts:383

---

### [ReactionType.Heart]

• **[ReactionType.Heart]**: _number_ = 0

Defined in types.ts:379

---

### [ReactionType.Thumb]

• **[ReactionType.Thumb]**: _number_ = 0

Defined in types.ts:377
