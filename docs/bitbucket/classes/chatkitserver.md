[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ChatKitServer](chatkitserver.md)

# Class: ChatKitServer

## Hierarchy

- **ChatKitServer**

## Index

### Properties

- [\_initialized](chatkitserver.md#markdown-header-static-private-_initialized)
- [apiKey](chatkitserver.md#markdown-header-static-apikey)
- [apiVersion](chatkitserver.md#markdown-header-static-apiversion)
- [appId](chatkitserver.md#markdown-header-static-appid)
- [httpClient](chatkitserver.md#markdown-header-static-private-httpclient)

### Methods

- [addUsersToConversation](chatkitserver.md#markdown-header-static-adduserstoconversation)
- [createGroupWithOwner](chatkitserver.md#markdown-header-static-creategroupwithowner)
- [deleteAllUsersInConversation](chatkitserver.md#markdown-header-static-deleteallusersinconversation)
- [deleteMessage](chatkitserver.md#markdown-header-static-deletemessage)
- [deleteUser](chatkitserver.md#markdown-header-static-deleteuser)
- [disableUser](chatkitserver.md#markdown-header-static-disableuser)
- [enableUser](chatkitserver.md#markdown-header-static-enableuser)
- [getConversationBasicUsers](chatkitserver.md#markdown-header-static-getconversationbasicusers)
- [getConversationConversableUsers](chatkitserver.md#markdown-header-static-getconversationconversableusers)
- [getConversationUsers](chatkitserver.md#markdown-header-static-getconversationusers)
- [getDefaultProfileImagesForGroup](chatkitserver.md#markdown-header-static-getdefaultprofileimagesforgroup)
- [getFile](chatkitserver.md#markdown-header-static-getfile)
- [getFileMessage](chatkitserver.md#markdown-header-static-getfilemessage)
- [getGroupProfileImage](chatkitserver.md#markdown-header-static-getgroupprofileimage)
- [getMessages](chatkitserver.md#markdown-header-static-getmessages)
- [getUserProfile](chatkitserver.md#markdown-header-static-getuserprofile)
- [getVideoThumbnail](chatkitserver.md#markdown-header-static-getvideothumbnail)
- [guard](chatkitserver.md#markdown-header-static-private-guard)
- [initialize](chatkitserver.md#markdown-header-static-initialize)
- [makeConversationOwner](chatkitserver.md#markdown-header-static-makeconversationowner)
- [removeConversationOwner](chatkitserver.md#markdown-header-static-removeconversationowner)
- [removeGroupImage](chatkitserver.md#markdown-header-static-removegroupimage)
- [removeUserProfileImage](chatkitserver.md#markdown-header-static-removeuserprofileimage)
- [removeUsersFromConversation](chatkitserver.md#markdown-header-static-removeusersfromconversation)
- [signInUserWithEmail](chatkitserver.md#markdown-header-static-signinuserwithemail)
- [signInUserWithId](chatkitserver.md#markdown-header-static-signinuserwithid)
- [signUpUser](chatkitserver.md#markdown-header-static-signupuser)
- [streamFile](chatkitserver.md#markdown-header-static-streamfile)
- [updateGroupImage](chatkitserver.md#markdown-header-static-updategroupimage)
- [updateGroupInfo](chatkitserver.md#markdown-header-static-updategroupinfo)
- [updateUserProfileImage](chatkitserver.md#markdown-header-static-updateuserprofileimage)
- [updateUserProfileInfo](chatkitserver.md#markdown-header-static-updateuserprofileinfo)
- [uploadFile](chatkitserver.md#markdown-header-static-uploadfile)
- [uploadFileWithStream](chatkitserver.md#markdown-header-static-uploadfilewithstream)
- [viewDefaultProfileImageForGroup](chatkitserver.md#markdown-header-static-viewdefaultprofileimageforgroup)

## Properties

### `Static` `Private` \_initialized

▪ **\_initialized**: _boolean_ = false

Defined in sayhey-chat-kit.ts:17

---

### `Static` apiKey

▪ **apiKey**: _string_

Defined in sayhey-chat-kit.ts:11

---

### `Static` apiVersion

▪ **apiVersion**: _string_ = API_VERSION

Defined in sayhey-chat-kit.ts:15

---

### `Static` appId

▪ **appId**: _string_

Defined in sayhey-chat-kit.ts:13

---

### `Static` `Private` httpClient

▪ **httpClient**: _AxiosInstance_

Defined in sayhey-chat-kit.ts:19

## Methods

### `Static` addUsersToConversation

▸ **addUsersToConversation**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:381

**Parameters:**

▪ **params**: _object_

| Name             | Type                               |
| ---------------- | ---------------------------------- |
| `conversationId` | string                             |
| `force?`         | undefined &#124; false &#124; true |
| `memberIds`      | string[]                           |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` createGroupWithOwner

▸ **createGroupWithOwner**(`params`: object): _Promise‹Result‹[GroupConversation](domain.groupconversation.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:479

**Parameters:**

▪ **params**: _object_

| Name             | Type                               |
| ---------------- | ---------------------------------- |
| `groupImageId`   | string &#124; null                 |
| `groupName`      | string                             |
| `memberIds`      | string[]                           |
| `ownerId`        | string                             |
| `serverManaged?` | undefined &#124; false &#124; true |

**Returns:** _Promise‹Result‹[GroupConversation](domain.groupconversation.md), [AppError](apperror.md)››_

---

### `Static` deleteAllUsersInConversation

▸ **deleteAllUsersInConversation**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:463

**Parameters:**

▪ **params**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` deleteMessage

▸ **deleteMessage**(`messageId`: string): _Promise‹Result‹[MessageDeleted](domain.messagedeleted.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:582

**Parameters:**

| Name        | Type   |
| ----------- | ------ |
| `messageId` | string |

**Returns:** _Promise‹Result‹[MessageDeleted](domain.messagedeleted.md), [AppError](apperror.md)››_

---

### `Static` deleteUser

▸ **deleteUser**(`userId`: string): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:296

**Parameters:**

| Name     | Type   |
| -------- | ------ |
| `userId` | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` disableUser

▸ **disableUser**(`userId`: string): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:270

**Parameters:**

| Name     | Type   |
| -------- | ------ |
| `userId` | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` enableUser

▸ **enableUser**(`userId`: string): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:283

**Parameters:**

| Name     | Type   |
| -------- | ------ |
| `userId` | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` getConversationBasicUsers

▸ **getConversationBasicUsers**(`params`: object): _Promise‹Result‹[ConversationUsersBasic](domain.conversationusersbasic.md)[], [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:361

**Parameters:**

▪ **params**: _object_

| Name             | Type                                    |
| ---------------- | --------------------------------------- |
| `conversationId` | string                                  |
| `type?`          | "all" &#124; "current" &#124; "default" |

**Returns:** _Promise‹Result‹[ConversationUsersBasic](domain.conversationusersbasic.md)[], [AppError](apperror.md)››_

---

### `Static` getConversationConversableUsers

▸ **getConversationConversableUsers**(`params`: object): _Promise‹Result‹[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:313

Conversation APIs

**Parameters:**

▪ **params**: _object_

| Name                    | Type                               |
| ----------------------- | ---------------------------------- |
| `conversationId`        | string                             |
| `includeRefIdInSearch?` | undefined &#124; false &#124; true |
| `limit?`                | undefined &#124; number            |
| `offset?`               | undefined &#124; number            |
| `search?`               | undefined &#124; string            |

**Returns:** _Promise‹Result‹[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md), [AppError](apperror.md)››_

---

### `Static` getConversationUsers

▸ **getConversationUsers**(`params`: object): _Promise‹Result‹[ConversationUsersBatch](domain.conversationusersbatch.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:336

**Parameters:**

▪ **params**: _object_

| Name                    | Type                                    |
| ----------------------- | --------------------------------------- |
| `conversationId`        | string                                  |
| `includeRefIdInSearch?` | undefined &#124; false &#124; true      |
| `limit?`                | undefined &#124; number                 |
| `offset?`               | undefined &#124; number                 |
| `search?`               | undefined &#124; string                 |
| `type?`                 | "all" &#124; "current" &#124; "default" |

**Returns:** _Promise‹Result‹[ConversationUsersBatch](domain.conversationusersbatch.md), [AppError](apperror.md)››_

---

### `Static` getDefaultProfileImagesForGroup

▸ **getDefaultProfileImagesForGroup**(): _Promise‹Result‹[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[], [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:114

App APIs

**Returns:** _Promise‹Result‹[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[], [AppError](apperror.md)››_

---

### `Static` getFile

▸ **getFile**(`url`: string, `method`: [HttpMethod](../enums/httpmethod.md)): _Promise‹Blob›_

Defined in sayhey-chat-kit.ts:64

**Parameters:**

| Name     | Type                                 |
| -------- | ------------------------------------ |
| `url`    | string                               |
| `method` | [HttpMethod](../enums/httpmethod.md) |

**Returns:** _Promise‹Blob›_

---

### `Static` getFileMessage

▸ **getFileMessage**‹**I**, **R**›(`options`: object): _Promise‹Result‹R, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:624

**Type parameters:**

▪ **I**: _boolean_

▪ **R**: _string | ReadStream_

**Parameters:**

▪ **options**: _object_

| Name              | Type   |
| ----------------- | ------ |
| `fileLocalIdName` | string |
| `messageId`       | string |
| `stream?`         | I      |

**Returns:** _Promise‹Result‹R, [AppError](apperror.md)››_

---

### `Static` getGroupProfileImage

▸ **getGroupProfileImage**‹**I**, **R**›(`options`: object): _Promise‹Result‹R, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:600

Media APIs

**Type parameters:**

▪ **I**: _boolean_

▪ **R**: _string | ReadStream_

**Parameters:**

▪ **options**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |
| `stream?`        | I      |

**Returns:** _Promise‹Result‹R, [AppError](apperror.md)››_

---

### `Static` getMessages

▸ **getMessages**(`params`: object): _Promise‹Result‹[Message](domain.message.md)[], [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:553

Message APIs

**Parameters:**

▪ **params**: _object_

| Name                | Type                                             |
| ------------------- | ------------------------------------------------ |
| `beforeSequence?`   | undefined &#124; string                          |
| `conversationId?`   | undefined &#124; string                          |
| `conversationType?` | [ConversationType](../enums/conversationtype.md) |
| `fromDate?`         | Date                                             |
| `galleryOnly?`      | undefined &#124; false &#124; true               |
| `includeDeleted?`   | undefined &#124; false &#124; true               |
| `limit?`            | undefined &#124; number                          |
| `search?`           | undefined &#124; string                          |
| `toDate?`           | Date                                             |
| `userId?`           | undefined &#124; string                          |

**Returns:** _Promise‹Result‹[Message](domain.message.md)[], [AppError](apperror.md)››_

---

### `Static` getUserProfile

▸ **getUserProfile**(`userId`: string): _Promise‹Result‹[UserSelf](domain.userself.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:205

**Parameters:**

| Name     | Type   |
| -------- | ------ |
| `userId` | string |

**Returns:** _Promise‹Result‹[UserSelf](domain.userself.md), [AppError](apperror.md)››_

---

### `Static` getVideoThumbnail

▸ **getVideoThumbnail**‹**I**, **R**›(`options`: object): _Promise‹Result‹R, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:652

**Type parameters:**

▪ **I**: _boolean_

▪ **R**: _string | ReadStream_

**Parameters:**

▪ **options**: _object_

| Name              | Type   |
| ----------------- | ------ |
| `fileLocalIdName` | string |
| `messageId`       | string |
| `stream?`         | I      |

**Returns:** _Promise‹Result‹R, [AppError](apperror.md)››_

---

### `Static` `Private` guard

▸ **guard**‹**T**›(`cb`: function): _Promise‹Result‹T, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:21

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: _function_

▸ (...`args`: any[]): _Promise‹T | [AppError](apperror.md)›_

**Parameters:**

| Name      | Type  |
| --------- | ----- |
| `...args` | any[] |

**Returns:** _Promise‹Result‹T, [AppError](apperror.md)››_

---

### `Static` initialize

▸ **initialize**(`options`: object): _void_

Defined in sayhey-chat-kit.ts:47

**Parameters:**

▪ **options**: _object_

| Name           | Type                    |
| -------------- | ----------------------- |
| `apiVersion?`  | undefined &#124; string |
| `sayheyApiKey` | string                  |
| `sayheyAppId`  | string                  |
| `sayheyUrl`    | string                  |

**Returns:** _void_

---

### `Static` makeConversationOwner

▸ **makeConversationOwner**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:429

**Parameters:**

▪ **params**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |
| `userId`         | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` removeConversationOwner

▸ **removeConversationOwner**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:446

**Parameters:**

▪ **params**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |
| `userId`         | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` removeGroupImage

▸ **removeGroupImage**(`conversationId`: string): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:537

**Parameters:**

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` removeUserProfileImage

▸ **removeUserProfileImage**(`userId`: string): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:257

**Parameters:**

| Name     | Type   |
| -------- | ------ |
| `userId` | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` removeUsersFromConversation

▸ **removeUsersFromConversation**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:405

**Parameters:**

▪ **params**: _object_

| Name             | Type                               |
| ---------------- | ---------------------------------- |
| `conversationId` | string                             |
| `force?`         | undefined &#124; false &#124; true |
| `memberIds`      | string                             |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` signInUserWithEmail

▸ **signInUserWithEmail**(`email`: string): _Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:173

**Parameters:**

| Name    | Type   |
| ------- | ------ |
| `email` | string |

**Returns:** _Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››_

---

### `Static` signInUserWithId

▸ **signInUserWithId**(`id`: string): _Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:189

**Parameters:**

| Name | Type   |
| ---- | ------ |
| `id` | string |

**Returns:** _Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››_

---

### `Static` signUpUser

▸ **signUpUser**(`params`: object): _Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:152

User APIs

**Parameters:**

▪ **params**: _object_

| Name              | Type                    |
| ----------------- | ----------------------- |
| `email`           | string                  |
| `firstName`       | string                  |
| `lastName`        | string                  |
| `password?`       | undefined &#124; string |
| `profileImageId?` | string &#124; null      |
| `refId?`          | undefined &#124; string |

**Returns:** _Promise‹Result‹[Auth](domain.auth.md), [AppError](apperror.md)››_

---

### `Static` streamFile

▸ **streamFile**(`url`: string, `method`: [HttpMethod](../enums/httpmethod.md)): _Promise‹ReadStream›_

Defined in sayhey-chat-kit.ts:73

**Parameters:**

| Name     | Type                                 |
| -------- | ------------------------------------ |
| `url`    | string                               |
| `method` | [HttpMethod](../enums/httpmethod.md) |

**Returns:** _Promise‹ReadStream›_

---

### `Static` updateGroupImage

▸ **updateGroupImage**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:519

**Parameters:**

▪ **params**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |
| `groupImageId`   | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` updateGroupInfo

▸ **updateGroupInfo**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:501

**Parameters:**

▪ **params**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `conversationId` | string |
| `groupName`      | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` updateUserProfileImage

▸ **updateUserProfileImage**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:239

**Parameters:**

▪ **params**: _object_

| Name             | Type   |
| ---------------- | ------ |
| `profileImageId` | string |
| `userId`         | string |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` updateUserProfileInfo

▸ **updateUserProfileInfo**(`params`: object): _Promise‹Result‹boolean, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:218

**Parameters:**

▪ **params**: _object_

| Name         | Type                    |
| ------------ | ----------------------- |
| `email?`     | undefined &#124; string |
| `firstName?` | undefined &#124; string |
| `lastName?`  | undefined &#124; string |
| `refId?`     | string &#124; null      |
| `userId`     | string                  |

**Returns:** _Promise‹Result‹boolean, [AppError](apperror.md)››_

---

### `Static` uploadFile

▸ **uploadFile**(`file`: Blob, `filename`: string): _Promise‹string›_

Defined in sayhey-chat-kit.ts:82

**Parameters:**

| Name       | Type   |
| ---------- | ------ |
| `file`     | Blob   |
| `filename` | string |

**Returns:** _Promise‹string›_

---

### `Static` uploadFileWithStream

▸ **uploadFileWithStream**(`readableStream`: ReadStream): _Promise‹string›_

Defined in sayhey-chat-kit.ts:96

**Parameters:**

| Name             | Type       |
| ---------------- | ---------- |
| `readableStream` | ReadStream |

**Returns:** _Promise‹string›_

---

### `Static` viewDefaultProfileImageForGroup

▸ **viewDefaultProfileImageForGroup**‹**I**, **R**›(`options`: object): _Promise‹Result‹R, [AppError](apperror.md)››_

Defined in sayhey-chat-kit.ts:129

**Type parameters:**

▪ **I**: _boolean_

▪ **R**: _Blob | ReadStream_

**Parameters:**

▪ **options**: _object_

| Name        | Type                               |
| ----------- | ---------------------------------- |
| `filename`  | string                             |
| `original?` | undefined &#124; false &#124; true |
| `stream?`   | I                                  |

**Returns:** _Promise‹Result‹R, [AppError](apperror.md)››_
