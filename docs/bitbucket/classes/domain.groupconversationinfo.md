[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [GroupConversationInfo](domain.groupconversationinfo.md)

# Class: GroupConversationInfo

## Hierarchy

- [ConversationInfo](domain.conversationinfo.md)

  ↳ **GroupConversationInfo**

## Index

### Properties

- [name](domain.groupconversationinfo.md#markdown-header-name)
- [picture](domain.groupconversationinfo.md#markdown-header-picture)

## Properties

### name

• **name**: _string_

_Inherited from [ConversationInfo](domain.conversationinfo.md).[name](domain.conversationinfo.md#markdown-header-name)_

Defined in types.ts:573

---

### picture

• **picture**: _[Media](../modules/domain.md#markdown-header-media) | null_

_Inherited from [ConversationInfo](domain.conversationinfo.md).[picture](domain.conversationinfo.md#markdown-header-picture)_

Defined in types.ts:571
