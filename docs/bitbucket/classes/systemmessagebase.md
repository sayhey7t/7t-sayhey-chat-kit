[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [SystemMessageBase](systemmessagebase.md)

# Class: SystemMessageBase

## Hierarchy

- **SystemMessageBase**

  ↳ [SystemMessage](dto.systemmessage.md)

  ↳ [SystemMessage](domain.systemmessage.md)

## Index

### Properties

- [actorCount](systemmessagebase.md#markdown-header-actorcount)
- [actors](systemmessagebase.md#markdown-header-actors)
- [id](systemmessagebase.md#markdown-header-id)
- [initiator](systemmessagebase.md#markdown-header-initiator)
- [refValue](systemmessagebase.md#markdown-header-refvalue)
- [systemMessageType](systemmessagebase.md#markdown-header-systemmessagetype)
- [text](systemmessagebase.md#markdown-header-optional-text)

## Properties

### actorCount

• **actorCount**: _number_

Defined in types.ts:349

---

### actors

• **actors**: _[UserSimpleInfo](usersimpleinfo.md)[]_

Defined in types.ts:353

---

### id

• **id**: _string_

Defined in types.ts:345

---

### initiator

• **initiator**: _[UserSimpleInfo](usersimpleinfo.md) | null_

Defined in types.ts:351

---

### refValue

• **refValue**: _string | null_

Defined in types.ts:347

---

### systemMessageType

• **systemMessageType**: _[SystemMessageType](../enums/systemmessagetype.md)_

Defined in types.ts:343

---

### `Optional` text

• **text**? : _string | null_

Defined in types.ts:355
