[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

# Class: ConversationConversableUsersBatchMeta

## Hierarchy

- [Paginatable](paginatable.md)

  ↳ **ConversationConversableUsersBatchMeta**

  ↳ [ConversationConversableUsersBatch](dto.conversationconversableusersbatch.md)

  ↳ [ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md)

## Index

### Properties

- [completed](conversationconversableusersbatchmeta.md#markdown-header-completed)
- [conversationId](conversationconversableusersbatchmeta.md#markdown-header-conversationid)
- [includeRefIdInSearch](conversationconversableusersbatchmeta.md#markdown-header-includerefidinsearch)
- [limit](conversationconversableusersbatchmeta.md#markdown-header-limit)
- [nextOffset](conversationconversableusersbatchmeta.md#markdown-header-nextoffset)
- [offset](conversationconversableusersbatchmeta.md#markdown-header-offset)
- [search](conversationconversableusersbatchmeta.md#markdown-header-search)
- [totalCount](conversationconversableusersbatchmeta.md#markdown-header-totalcount)

## Properties

### completed

• **completed**: _boolean_

_Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)_

Defined in types.ts:241

---

### conversationId

• **conversationId**: _string_

Defined in types.ts:247

---

### includeRefIdInSearch

• **includeRefIdInSearch**: _boolean_

Defined in types.ts:245

---

### limit

• **limit**: _number_

_Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)_

Defined in types.ts:239

---

### nextOffset

• **nextOffset**: _number_

_Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)_

Defined in types.ts:237

---

### offset

• **offset**: _number_

_Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)_

Defined in types.ts:235

---

### search

• **search**: _string | null_

_Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)_

Defined in types.ts:231

---

### totalCount

• **totalCount**: _number_

_Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)_

Defined in types.ts:233
