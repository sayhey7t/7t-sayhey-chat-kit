[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ImageBase](imagebase.md)

# Class: ImageBase

## Hierarchy

- [MediaBase](mediabase.md)

  ↳ **ImageBase**

## Index

### Properties

- [encoding](imagebase.md#markdown-header-encoding)
- [extension](imagebase.md#markdown-header-extension)
- [fileSize](imagebase.md#markdown-header-filesize)
- [fileType](imagebase.md#markdown-header-filetype)
- [filename](imagebase.md#markdown-header-filename)
- [imageInfo](imagebase.md#markdown-header-imageinfo)
- [mimeType](imagebase.md#markdown-header-mimetype)
- [urlPath](imagebase.md#markdown-header-urlpath)
- [videoInfo](imagebase.md#markdown-header-videoinfo)

## Properties

### encoding

• **encoding**: _string_

_Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)_

Defined in types.ts:273

---

### extension

• **extension**: _string_

_Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)_

Defined in types.ts:271

---

### fileSize

• **fileSize**: _number_

_Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)_

Defined in types.ts:277

---

### fileType

• **fileType**: _[FileType](../enums/filetype.md)_ = FileType.Image

Defined in types.ts:281

---

### filename

• **filename**: _string_

_Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)_

Defined in types.ts:275

---

### imageInfo

• **imageInfo**: _[ImageInfoMedia](imageinfomedia.md)_

Defined in types.ts:283

---

### mimeType

• **mimeType**: _string_

_Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)_

Defined in types.ts:269

---

### urlPath

• **urlPath**: _string_

_Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)_

Defined in types.ts:267

---

### videoInfo

• **videoInfo**: _null_ = null

Defined in types.ts:285
