[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationUsersBatchBase](conversationusersbatchbase.md)

# Class: ConversationUsersBatchBase

## Hierarchy

↳ [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

↳ **ConversationUsersBatchBase**

↳ [ConversationUsersBatch](dto.conversationusersbatch.md)

↳ [ConversationUsersBatch](domain.conversationusersbatch.md)

## Index

### Properties

- [completed](conversationusersbatchbase.md#markdown-header-completed)
- [conversationId](conversationusersbatchbase.md#markdown-header-conversationid)
- [includeRefIdInSearch](conversationusersbatchbase.md#markdown-header-includerefidinsearch)
- [limit](conversationusersbatchbase.md#markdown-header-limit)
- [nextOffset](conversationusersbatchbase.md#markdown-header-nextoffset)
- [offset](conversationusersbatchbase.md#markdown-header-offset)
- [search](conversationusersbatchbase.md#markdown-header-search)
- [totalCount](conversationusersbatchbase.md#markdown-header-totalcount)
- [type](conversationusersbatchbase.md#markdown-header-type)

## Properties

### completed

• **completed**: _boolean_

_Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)_

Defined in types.ts:241

---

### conversationId

• **conversationId**: _string_

_Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[conversationId](conversationusersbatchmeta.md#markdown-header-conversationid)_

Defined in types.ts:253

---

### includeRefIdInSearch

• **includeRefIdInSearch**: _boolean_

_Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[includeRefIdInSearch](conversationusersbatchmeta.md#markdown-header-includerefidinsearch)_

Defined in types.ts:251

---

### limit

• **limit**: _number_

_Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)_

Defined in types.ts:239

---

### nextOffset

• **nextOffset**: _number_

_Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)_

Defined in types.ts:237

---

### offset

• **offset**: _number_

_Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)_

Defined in types.ts:235

---

### search

• **search**: _string | null_

_Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)_

Defined in types.ts:231

---

### totalCount

• **totalCount**: _number_

_Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)_

Defined in types.ts:233

---

### type

• **type**: _[ConversationUserQueryType](../enums/conversationuserquerytype.md)_

Defined in types.ts:263
