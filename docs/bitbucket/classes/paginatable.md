[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Paginatable](paginatable.md)

# Class: Paginatable

## Hierarchy

- **Paginatable**

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

## Index

### Properties

- [completed](paginatable.md#markdown-header-completed)
- [limit](paginatable.md#markdown-header-limit)
- [nextOffset](paginatable.md#markdown-header-nextoffset)
- [offset](paginatable.md#markdown-header-offset)
- [search](paginatable.md#markdown-header-search)
- [totalCount](paginatable.md#markdown-header-totalcount)

## Properties

### completed

• **completed**: _boolean_

Defined in types.ts:241

---

### limit

• **limit**: _number_

Defined in types.ts:239

---

### nextOffset

• **nextOffset**: _number_

Defined in types.ts:237

---

### offset

• **offset**: _number_

Defined in types.ts:235

---

### search

• **search**: _string | null_

Defined in types.ts:231

---

### totalCount

• **totalCount**: _number_

Defined in types.ts:233
