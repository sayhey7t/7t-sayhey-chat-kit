[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AuthBase](authbase.md)

# Class: AuthBase

## Hierarchy

- **AuthBase**

  ↳ [Auth](dto.auth.md)

  ↳ [Auth](domain.auth.md)

## Index

### Properties

- [accessToken](authbase.md#markdown-header-accesstoken)
- [publisherToken](authbase.md#markdown-header-publishertoken)
- [refreshToken](authbase.md#markdown-header-refreshtoken)

## Properties

### accessToken

• **accessToken**: _string_

Defined in types.ts:176

---

### publisherToken

• **publisherToken**: _string_

Defined in types.ts:178

---

### refreshToken

• **refreshToken**: _string_

Defined in types.ts:180
