[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationInfo](domain.conversationinfo.md)

# Class: ConversationInfo

## Hierarchy

- **ConversationInfo**

  ↳ [GroupConversationInfo](domain.groupconversationinfo.md)

  ↳ [UserConversationInfo](domain.userconversationinfo.md)

## Index

### Properties

- [name](domain.conversationinfo.md#markdown-header-name)
- [picture](domain.conversationinfo.md#markdown-header-picture)

## Properties

### name

• **name**: _string_

Defined in types.ts:573

---

### picture

• **picture**: _[Media](../modules/domain.md#markdown-header-media) | null_

Defined in types.ts:571
