[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MediaBase](mediabase.md)

# Class: MediaBase

## Hierarchy

- **MediaBase**

  ↳ [ImageBase](imagebase.md)

  ↳ [VideoBase](videobase.md)

  ↳ [AudioBase](audiobase.md)

  ↳ [DocumentBase](documentbase.md)

## Index

### Properties

- [encoding](mediabase.md#markdown-header-encoding)
- [extension](mediabase.md#markdown-header-extension)
- [fileSize](mediabase.md#markdown-header-filesize)
- [filename](mediabase.md#markdown-header-filename)
- [mimeType](mediabase.md#markdown-header-mimetype)
- [urlPath](mediabase.md#markdown-header-urlpath)

## Properties

### encoding

• **encoding**: _string_

Defined in types.ts:273

---

### extension

• **extension**: _string_

Defined in types.ts:271

---

### fileSize

• **fileSize**: _number_

Defined in types.ts:277

---

### filename

• **filename**: _string_

Defined in types.ts:275

---

### mimeType

• **mimeType**: _string_

Defined in types.ts:269

---

### urlPath

• **urlPath**: _string_

Defined in types.ts:267
