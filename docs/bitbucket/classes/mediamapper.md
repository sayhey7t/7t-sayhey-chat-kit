[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MediaMapper](mediamapper.md)

# Class: MediaMapper

## Hierarchy

- **MediaMapper**

## Index

### Methods

- [toMediaDomain](mediamapper.md#markdown-header-static-tomediadomain)

## Methods

### `Static` toMediaDomain

▸ **toMediaDomain**(`media`: [Media](../modules/dto.md#markdown-header-media)): _[Media](../modules/domain.md#markdown-header-media)_

Defined in mapper.ts:46

**Parameters:**

| Name    | Type                                             |
| ------- | ------------------------------------------------ |
| `media` | [Media](../modules/dto.md#markdown-header-media) |

**Returns:** _[Media](../modules/domain.md#markdown-header-media)_
