[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserSimpleInfo](usersimpleinfo.md)

# Class: UserSimpleInfo

## Hierarchy

- **UserSimpleInfo**

## Index

### Properties

- [firstName](usersimpleinfo.md#markdown-header-firstname)
- [id](usersimpleinfo.md#markdown-header-id)
- [lastName](usersimpleinfo.md#markdown-header-lastname)
- [refId](usersimpleinfo.md#markdown-header-refid)

## Properties

### firstName

• **firstName**: _string_

Defined in types.ts:333

---

### id

• **id**: _string_

Defined in types.ts:337

---

### lastName

• **lastName**: _string_

Defined in types.ts:335

---

### refId

• **refId**: _string | null_

Defined in types.ts:339
