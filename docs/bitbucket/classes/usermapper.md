[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserMapper](usermapper.md)

# Class: UserMapper

## Hierarchy

- **UserMapper**

## Index

### Methods

- [toConversationUserBasicDomain](usermapper.md#markdown-header-static-toconversationuserbasicdomain)
- [toConversationUserDomain](usermapper.md#markdown-header-static-toconversationuserdomain)
- [toUserDomain](usermapper.md#markdown-header-static-touserdomain)
- [toUserSelfDomain](usermapper.md#markdown-header-static-touserselfdomain)

## Methods

### `Static` toConversationUserBasicDomain

▸ **toConversationUserBasicDomain**(`user`: [ConversationUsersBasic](dto.conversationusersbasic.md)): _[ConversationUsersBasic](domain.conversationusersbasic.md)_

Defined in mapper.ts:31

**Parameters:**

| Name   | Type                                                    |
| ------ | ------------------------------------------------------- |
| `user` | [ConversationUsersBasic](dto.conversationusersbasic.md) |

**Returns:** _[ConversationUsersBasic](domain.conversationusersbasic.md)_

---

### `Static` toConversationUserDomain

▸ **toConversationUserDomain**(`user`: [ConversationUser](dto.conversationuser.md)): _[ConversationUser](domain.conversationuser.md)_

Defined in mapper.ts:18

**Parameters:**

| Name   | Type                                        |
| ------ | ------------------------------------------- |
| `user` | [ConversationUser](dto.conversationuser.md) |

**Returns:** _[ConversationUser](domain.conversationuser.md)_

---

### `Static` toUserDomain

▸ **toUserDomain**(`user`: [User](dto.user.md)): _[User](domain.user.md)_

Defined in mapper.ts:12

**Parameters:**

| Name   | Type                |
| ------ | ------------------- |
| `user` | [User](dto.user.md) |

**Returns:** _[User](domain.user.md)_

---

### `Static` toUserSelfDomain

▸ **toUserSelfDomain**(`user`: [UserSelf](dto.userself.md)): _[UserSelf](domain.userself.md)_

Defined in mapper.ts:26

**Parameters:**

| Name   | Type                        |
| ------ | --------------------------- |
| `user` | [UserSelf](dto.userself.md) |

**Returns:** _[UserSelf](domain.userself.md)_
