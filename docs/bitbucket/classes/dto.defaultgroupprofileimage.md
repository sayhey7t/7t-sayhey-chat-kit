[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)

# Class: DefaultGroupProfileImage

## Hierarchy

- [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md)

  ↳ **DefaultGroupProfileImage**

## Index

### Properties

- [filename](dto.defaultgroupprofileimage.md#markdown-header-filename)
- [original](dto.defaultgroupprofileimage.md#markdown-header-original)
- [thumbnail](dto.defaultgroupprofileimage.md#markdown-header-thumbnail)

## Properties

### filename

• **filename**: _string_

_Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[filename](defaultgroupprofileimagebase.md#markdown-header-filename)_

Defined in types.ts:168

---

### original

• **original**: _string_

_Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[original](defaultgroupprofileimagebase.md#markdown-header-original)_

Defined in types.ts:170

---

### thumbnail

• **thumbnail**: _string_

_Inherited from [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md).[thumbnail](defaultgroupprofileimagebase.md#markdown-header-thumbnail)_

Defined in types.ts:172
