[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [SystemMessage](dto.systemmessage.md)

# Class: SystemMessage

## Hierarchy

- [SystemMessageBase](systemmessagebase.md)

  ↳ **SystemMessage**

## Index

### Properties

- [actorCount](dto.systemmessage.md#markdown-header-actorcount)
- [actors](dto.systemmessage.md#markdown-header-actors)
- [id](dto.systemmessage.md#markdown-header-id)
- [initiator](dto.systemmessage.md#markdown-header-initiator)
- [refValue](dto.systemmessage.md#markdown-header-refvalue)
- [systemMessageType](dto.systemmessage.md#markdown-header-systemmessagetype)
- [text](dto.systemmessage.md#markdown-header-optional-text)

## Properties

### actorCount

• **actorCount**: _number_

_Inherited from [SystemMessageBase](systemmessagebase.md).[actorCount](systemmessagebase.md#markdown-header-actorcount)_

Defined in types.ts:349

---

### actors

• **actors**: _[UserSimpleInfo](usersimpleinfo.md)[]_

_Inherited from [SystemMessageBase](systemmessagebase.md).[actors](systemmessagebase.md#markdown-header-actors)_

Defined in types.ts:353

---

### id

• **id**: _string_

_Inherited from [SystemMessageBase](systemmessagebase.md).[id](systemmessagebase.md#markdown-header-id)_

Defined in types.ts:345

---

### initiator

• **initiator**: _[UserSimpleInfo](usersimpleinfo.md) | null_

_Inherited from [SystemMessageBase](systemmessagebase.md).[initiator](systemmessagebase.md#markdown-header-initiator)_

Defined in types.ts:351

---

### refValue

• **refValue**: _string | null_

_Inherited from [SystemMessageBase](systemmessagebase.md).[refValue](systemmessagebase.md#markdown-header-refvalue)_

Defined in types.ts:347

---

### systemMessageType

• **systemMessageType**: _[SystemMessageType](../enums/systemmessagetype.md)_

_Inherited from [SystemMessageBase](systemmessagebase.md).[systemMessageType](systemmessagebase.md#markdown-header-systemmessagetype)_

Defined in types.ts:343

---

### `Optional` text

• **text**? : _string | null_

_Inherited from [SystemMessageBase](systemmessagebase.md).[text](systemmessagebase.md#markdown-header-optional-text)_

Defined in types.ts:355
