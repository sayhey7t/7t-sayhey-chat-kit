[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationUsersBatch](dto.conversationusersbatch.md)

# Class: ConversationUsersBatch

## Hierarchy

↳ [ConversationUsersBatchBase](conversationusersbatchbase.md)

↳ **ConversationUsersBatch**

## Index

### Properties

- [completed](dto.conversationusersbatch.md#markdown-header-completed)
- [conversationId](dto.conversationusersbatch.md#markdown-header-conversationid)
- [includeRefIdInSearch](dto.conversationusersbatch.md#markdown-header-includerefidinsearch)
- [limit](dto.conversationusersbatch.md#markdown-header-limit)
- [nextOffset](dto.conversationusersbatch.md#markdown-header-nextoffset)
- [offset](dto.conversationusersbatch.md#markdown-header-offset)
- [results](dto.conversationusersbatch.md#markdown-header-results)
- [search](dto.conversationusersbatch.md#markdown-header-search)
- [totalCount](dto.conversationusersbatch.md#markdown-header-totalcount)
- [type](dto.conversationusersbatch.md#markdown-header-type)

## Properties

### completed

• **completed**: _boolean_

_Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)_

Defined in types.ts:241

---

### conversationId

• **conversationId**: _string_

_Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[conversationId](conversationusersbatchmeta.md#markdown-header-conversationid)_

Defined in types.ts:253

---

### includeRefIdInSearch

• **includeRefIdInSearch**: _boolean_

_Inherited from [ConversationUsersBatchMeta](conversationusersbatchmeta.md).[includeRefIdInSearch](conversationusersbatchmeta.md#markdown-header-includerefidinsearch)_

Defined in types.ts:251

---

### limit

• **limit**: _number_

_Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)_

Defined in types.ts:239

---

### nextOffset

• **nextOffset**: _number_

_Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)_

Defined in types.ts:237

---

### offset

• **offset**: _number_

_Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)_

Defined in types.ts:235

---

### results

• **results**: _[ConversationUser](dto.conversationuser.md)[]_

Defined in types.ts:448

---

### search

• **search**: _string | null_

_Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)_

Defined in types.ts:231

---

### totalCount

• **totalCount**: _number_

_Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)_

Defined in types.ts:233

---

### type

• **type**: _[ConversationUserQueryType](../enums/conversationuserquerytype.md)_

_Inherited from [ConversationUsersBatchBase](conversationusersbatchbase.md).[type](conversationusersbatchbase.md#markdown-header-type)_

Defined in types.ts:263
