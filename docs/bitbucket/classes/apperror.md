[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AppError](apperror.md)

# Class: AppError

## Hierarchy

- **AppError**

## Index

### Constructors

- [constructor](apperror.md#markdown-header-constructor)

### Properties

- [authFailed](apperror.md#markdown-header-optional-authfailed)
- [error](apperror.md#markdown-header-optional-error)
- [exchange](apperror.md#markdown-header-optional-exchange)
- [message](apperror.md#markdown-header-optional-message)
- [statusCode](apperror.md#markdown-header-optional-statuscode)
- [type](apperror.md#markdown-header-type)

### Methods

- [setErrorActions](apperror.md#markdown-header-seterroractions)
- [setMessage](apperror.md#markdown-header-setmessage)
- [setStatusCode](apperror.md#markdown-header-setstatuscode)

## Constructors

### constructor

\+ **new AppError**(`err?`: string | Error, `type?`: [Type](../modules/errortype.md#markdown-header-type)): _[AppError](apperror.md)_

Defined in types.ts:144

**Parameters:**

| Name    | Type                                                 |
| ------- | ---------------------------------------------------- |
| `err?`  | string &#124; Error                                  |
| `type?` | [Type](../modules/errortype.md#markdown-header-type) |

**Returns:** _[AppError](apperror.md)_

## Properties

### `Optional` authFailed

• **authFailed**? : _undefined | false | true_

Defined in types.ts:136

---

### `Optional` error

• **error**? : _Error_

Defined in types.ts:132

---

### `Optional` exchange

• **exchange**? : _undefined | false | true_

Defined in types.ts:138

---

### `Optional` message

• **message**? : _undefined | string_

Defined in types.ts:134

---

### `Optional` statusCode

• **statusCode**? : _undefined | number_

Defined in types.ts:130

---

### type

• **type**: _[Type](../modules/errortype.md#markdown-header-type)_ = ErrorType.InternalErrorType.Unknown

Defined in types.ts:128

## Methods

### setErrorActions

▸ **setErrorActions**(`params`: object): _void_

Defined in types.ts:140

**Parameters:**

▪ **params**: _object_

| Name          | Type                               |
| ------------- | ---------------------------------- |
| `authFailed?` | undefined &#124; false &#124; true |
| `exchange?`   | undefined &#124; false &#124; true |

**Returns:** _void_

---

### setMessage

▸ **setMessage**(`msg`: string): _void_

Defined in types.ts:162

**Parameters:**

| Name  | Type   |
| ----- | ------ |
| `msg` | string |

**Returns:** _void_

---

### setStatusCode

▸ **setStatusCode**(`statusCode?`: undefined | number): _void_

Defined in types.ts:158

**Parameters:**

| Name          | Type                    |
| ------------- | ----------------------- |
| `statusCode?` | undefined &#124; number |

**Returns:** _void_
