[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [UserConversationInfo](domain.userconversationinfo.md)

# Class: UserConversationInfo

## Hierarchy

- [ConversationInfo](domain.conversationinfo.md)

  ↳ **UserConversationInfo**

## Index

### Properties

- [email](domain.userconversationinfo.md#markdown-header-email)
- [id](domain.userconversationinfo.md#markdown-header-id)
- [name](domain.userconversationinfo.md#markdown-header-name)
- [picture](domain.userconversationinfo.md#markdown-header-picture)
- [refId](domain.userconversationinfo.md#markdown-header-refid)

## Properties

### email

• **email**: _string_

Defined in types.ts:579

---

### id

• **id**: _string_

Defined in types.ts:577

---

### name

• **name**: _string_

_Inherited from [ConversationInfo](domain.conversationinfo.md).[name](domain.conversationinfo.md#markdown-header-name)_

Defined in types.ts:573

---

### picture

• **picture**: _[Media](../modules/domain.md#markdown-header-media) | null_

_Inherited from [ConversationInfo](domain.conversationinfo.md).[picture](domain.conversationinfo.md#markdown-header-picture)_

Defined in types.ts:571

---

### refId

• **refId**: _string | null_

Defined in types.ts:581
