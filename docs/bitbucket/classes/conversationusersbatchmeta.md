[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

# Class: ConversationUsersBatchMeta

## Hierarchy

- [Paginatable](paginatable.md)

  ↳ **ConversationUsersBatchMeta**

  ↳ [ConversationUsersBatchBase](conversationusersbatchbase.md)

## Index

### Properties

- [completed](conversationusersbatchmeta.md#markdown-header-completed)
- [conversationId](conversationusersbatchmeta.md#markdown-header-conversationid)
- [includeRefIdInSearch](conversationusersbatchmeta.md#markdown-header-includerefidinsearch)
- [limit](conversationusersbatchmeta.md#markdown-header-limit)
- [nextOffset](conversationusersbatchmeta.md#markdown-header-nextoffset)
- [offset](conversationusersbatchmeta.md#markdown-header-offset)
- [search](conversationusersbatchmeta.md#markdown-header-search)
- [totalCount](conversationusersbatchmeta.md#markdown-header-totalcount)

## Properties

### completed

• **completed**: _boolean_

_Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)_

Defined in types.ts:241

---

### conversationId

• **conversationId**: _string_

Defined in types.ts:253

---

### includeRefIdInSearch

• **includeRefIdInSearch**: _boolean_

Defined in types.ts:251

---

### limit

• **limit**: _number_

_Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)_

Defined in types.ts:239

---

### nextOffset

• **nextOffset**: _number_

_Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)_

Defined in types.ts:237

---

### offset

• **offset**: _number_

_Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)_

Defined in types.ts:235

---

### search

• **search**: _string | null_

_Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)_

Defined in types.ts:231

---

### totalCount

• **totalCount**: _number_

_Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)_

Defined in types.ts:233
