[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MessageMapper](messagemapper.md)

# Class: MessageMapper

## Hierarchy

- **MessageMapper**

## Index

### Methods

- [toMessageDeletedDomain](messagemapper.md#markdown-header-static-tomessagedeleteddomain)
- [toMessageDomain](messagemapper.md#markdown-header-static-tomessagedomain)

## Methods

### `Static` toMessageDeletedDomain

▸ **toMessageDeletedDomain**(`messageEvent`: [MessageDeleted](dto.messagedeleted.md)): _[MessageDeleted](domain.messagedeleted.md)_

Defined in mapper.ts:63

**Parameters:**

| Name           | Type                                    |
| -------------- | --------------------------------------- |
| `messageEvent` | [MessageDeleted](dto.messagedeleted.md) |

**Returns:** _[MessageDeleted](domain.messagedeleted.md)_

---

### `Static` toMessageDomain

▸ **toMessageDomain**(`message`: [Message](dto.message.md)): _[Message](domain.message.md)_

Defined in mapper.ts:52

**Parameters:**

| Name      | Type                      |
| --------- | ------------------------- |
| `message` | [Message](dto.message.md) |

**Returns:** _[Message](domain.message.md)_
