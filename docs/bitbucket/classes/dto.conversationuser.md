[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationUser](dto.conversationuser.md)

# Class: ConversationUser

## Hierarchy

↳ [User](dto.user.md)

↳ **ConversationUser**

## Index

### Properties

- [deletedAt](dto.conversationuser.md#markdown-header-deletedat)
- [deletedFromConversationAt](dto.conversationuser.md#markdown-header-deletedfromconversationat)
- [disabled](dto.conversationuser.md#markdown-header-disabled)
- [email](dto.conversationuser.md#markdown-header-email)
- [firstName](dto.conversationuser.md#markdown-header-firstname)
- [id](dto.conversationuser.md#markdown-header-id)
- [isOwner](dto.conversationuser.md#markdown-header-isowner)
- [lastName](dto.conversationuser.md#markdown-header-lastname)
- [picture](dto.conversationuser.md#markdown-header-picture)
- [refId](dto.conversationuser.md#markdown-header-refid)

## Properties

### deletedAt

• **deletedAt**: _string | null_

_Inherited from [User](dto.user.md).[deletedAt](dto.user.md#markdown-header-deletedat)_

Defined in types.ts:434

---

### deletedFromConversationAt

• **deletedFromConversationAt**: _string | null_

Defined in types.ts:445

---

### disabled

• **disabled**: _boolean_

_Inherited from [User](dto.user.md).[disabled](dto.user.md#markdown-header-disabled)_

Defined in types.ts:432

---

### email

• **email**: _string_

_Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)_

Defined in types.ts:194

---

### firstName

• **firstName**: _string_

_Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)_

Defined in types.ts:186

---

### id

• **id**: _string_

_Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)_

Defined in types.ts:184

---

### isOwner

• **isOwner**: _boolean_

Defined in types.ts:443

---

### lastName

• **lastName**: _string_

_Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)_

Defined in types.ts:188

---

### picture

• **picture**: _[Media](../modules/dto.md#markdown-header-media) | null_

_Inherited from [User](dto.user.md).[picture](dto.user.md#markdown-header-picture)_

Defined in types.ts:430

---

### refId

• **refId**: _string | null_

_Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)_

Defined in types.ts:190
