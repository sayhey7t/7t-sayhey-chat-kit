[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [Conversation](dto.conversation.md)

# Class: Conversation

## Hierarchy

- [ConversationBase](conversationbase.md)

  ↳ **Conversation**

  ↳ [GroupConversation](dto.groupconversation.md)

  ↳ [IndividualConversation](dto.individualconversation.md)

## Index

### Properties

- [badge](dto.conversation.md#markdown-header-badge)
- [createdAt](dto.conversation.md#markdown-header-createdat)
- [deletedFromConversationAt](dto.conversation.md#markdown-header-deletedfromconversationat)
- [endBefore](dto.conversation.md#markdown-header-endbefore)
- [group](dto.conversation.md#markdown-header-optional-group)
- [id](dto.conversation.md#markdown-header-id)
- [isOwner](dto.conversation.md#markdown-header-isowner)
- [lastMessage](dto.conversation.md#markdown-header-lastmessage)
- [lastReadMessageId](dto.conversation.md#markdown-header-lastreadmessageid)
- [muteUntil](dto.conversation.md#markdown-header-muteuntil)
- [pinnedAt](dto.conversation.md#markdown-header-pinnedat)
- [serverCreated](dto.conversation.md#markdown-header-servercreated)
- [serverManaged](dto.conversation.md#markdown-header-servermanaged)
- [startAfter](dto.conversation.md#markdown-header-startafter)
- [type](dto.conversation.md#markdown-header-type)
- [updatedAt](dto.conversation.md#markdown-header-updatedat)
- [user](dto.conversation.md#markdown-header-optional-user)
- [visible](dto.conversation.md#markdown-header-visible)

## Properties

### badge

• **badge**: _number | null_

Defined in types.ts:482

---

### createdAt

• **createdAt**: _string_

Defined in types.ts:490

---

### deletedFromConversationAt

• **deletedFromConversationAt**: _string | null_

Defined in types.ts:488

---

### endBefore

• **endBefore**: _string | null_

Defined in types.ts:486

---

### `Optional` group

• **group**? : _[ConversationInfo](dto.conversationinfo.md)_

Defined in types.ts:496

---

### id

• **id**: _string_

_Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)_

Defined in types.ts:410

---

### isOwner

• **isOwner**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)_

Defined in types.ts:416

---

### lastMessage

• **lastMessage**: _[Message](dto.message.md) | null_

Defined in types.ts:494

---

### lastReadMessageId

• **lastReadMessageId**: _string | null_

_Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)_

Defined in types.ts:412

---

### muteUntil

• **muteUntil**: _string | null_

Defined in types.ts:478

---

### pinnedAt

• **pinnedAt**: _string | null_

Defined in types.ts:480

---

### serverCreated

• **serverCreated**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)_

Defined in types.ts:420

---

### serverManaged

• **serverManaged**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)_

Defined in types.ts:422

---

### startAfter

• **startAfter**: _string | null_

Defined in types.ts:484

---

### type

• **type**: _[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)_

_Inherited from [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)_

Defined in types.ts:414

---

### updatedAt

• **updatedAt**: _string_

Defined in types.ts:492

---

### `Optional` user

• **user**? : _[UserConversationInfo](dto.userconversationinfo.md)_

Defined in types.ts:498

---

### visible

• **visible**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)_

Defined in types.ts:418
