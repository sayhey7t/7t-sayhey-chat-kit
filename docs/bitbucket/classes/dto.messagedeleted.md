[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [MessageDeleted](dto.messagedeleted.md)

# Class: MessageDeleted

## Hierarchy

- **MessageDeleted**

## Index

### Properties

- [conversationId](dto.messagedeleted.md#markdown-header-conversationid)
- [deletedAt](dto.messagedeleted.md#markdown-header-deletedat)
- [messageId](dto.messagedeleted.md#markdown-header-messageid)

## Properties

### conversationId

• **conversationId**: _string_

Defined in types.ts:512

---

### deletedAt

• **deletedAt**: _string_

Defined in types.ts:516

---

### messageId

• **messageId**: _string_

Defined in types.ts:514
