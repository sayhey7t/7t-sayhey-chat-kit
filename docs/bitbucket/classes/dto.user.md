[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [User](dto.user.md)

# Class: User

## Hierarchy

↳ [UserDetail](userdetail.md)

↳ **User**

↳ [UserSelf](dto.userself.md)

↳ [ConversationUser](dto.conversationuser.md)

## Index

### Properties

- [deletedAt](dto.user.md#markdown-header-deletedat)
- [disabled](dto.user.md#markdown-header-disabled)
- [email](dto.user.md#markdown-header-email)
- [firstName](dto.user.md#markdown-header-firstname)
- [id](dto.user.md#markdown-header-id)
- [lastName](dto.user.md#markdown-header-lastname)
- [picture](dto.user.md#markdown-header-picture)
- [refId](dto.user.md#markdown-header-refid)

## Properties

### deletedAt

• **deletedAt**: _string | null_

Defined in types.ts:434

---

### disabled

• **disabled**: _boolean_

Defined in types.ts:432

---

### email

• **email**: _string_

_Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)_

Defined in types.ts:194

---

### firstName

• **firstName**: _string_

_Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)_

Defined in types.ts:186

---

### id

• **id**: _string_

_Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)_

Defined in types.ts:184

---

### lastName

• **lastName**: _string_

_Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)_

Defined in types.ts:188

---

### picture

• **picture**: _[Media](../modules/dto.md#markdown-header-media) | null_

Defined in types.ts:430

---

### refId

• **refId**: _string | null_

_Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)_

Defined in types.ts:190
