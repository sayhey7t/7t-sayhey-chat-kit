[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [ConversationUsersBasic](domain.conversationusersbasic.md)

# Class: ConversationUsersBasic

## Hierarchy

- **ConversationUsersBasic**

## Index

### Properties

- [deletedFromConversationAt](domain.conversationusersbasic.md#markdown-header-deletedfromconversationat)
- [id](domain.conversationusersbasic.md#markdown-header-id)
- [isOwner](domain.conversationusersbasic.md#markdown-header-isowner)
- [userDeletedAt](domain.conversationusersbasic.md#markdown-header-userdeletedat)
- [userDisabled](domain.conversationusersbasic.md#markdown-header-userdisabled)

## Properties

### deletedFromConversationAt

• **deletedFromConversationAt**: _Date | null_

Defined in types.ts:625

---

### id

• **id**: _string_

Defined in types.ts:623

---

### isOwner

• **isOwner**: _boolean_

Defined in types.ts:621

---

### userDeletedAt

• **userDeletedAt**: _Date | null_

Defined in types.ts:629

---

### userDisabled

• **userDisabled**: _boolean_

Defined in types.ts:627
