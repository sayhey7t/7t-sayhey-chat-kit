[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [Conversation](domain.conversation.md)

# Class: Conversation

## Hierarchy

- [ConversationBase](conversationbase.md)

  ↳ **Conversation**

  ↳ [GroupConversation](domain.groupconversation.md)

  ↳ [IndividualConversation](domain.individualconversation.md)

## Index

### Properties

- [badge](domain.conversation.md#markdown-header-badge)
- [createdAt](domain.conversation.md#markdown-header-createdat)
- [deletedFromConversationAt](domain.conversation.md#markdown-header-deletedfromconversationat)
- [endBefore](domain.conversation.md#markdown-header-endbefore)
- [id](domain.conversation.md#markdown-header-id)
- [isOwner](domain.conversation.md#markdown-header-isowner)
- [lastMessage](domain.conversation.md#markdown-header-lastmessage)
- [lastReadMessageId](domain.conversation.md#markdown-header-lastreadmessageid)
- [muteUntil](domain.conversation.md#markdown-header-muteuntil)
- [pinnedAt](domain.conversation.md#markdown-header-pinnedat)
- [serverCreated](domain.conversation.md#markdown-header-servercreated)
- [serverManaged](domain.conversation.md#markdown-header-servermanaged)
- [startAfter](domain.conversation.md#markdown-header-startafter)
- [type](domain.conversation.md#markdown-header-type)
- [updatedAt](domain.conversation.md#markdown-header-updatedat)
- [visible](domain.conversation.md#markdown-header-visible)

## Properties

### badge

• **badge**: _number | null_

Defined in types.ts:588

---

### createdAt

• **createdAt**: _Date_

Defined in types.ts:596

---

### deletedFromConversationAt

• **deletedFromConversationAt**: _Date | null_

Defined in types.ts:594

---

### endBefore

• **endBefore**: _Date | null_

Defined in types.ts:592

---

### id

• **id**: _string_

_Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)_

Defined in types.ts:410

---

### isOwner

• **isOwner**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)_

Defined in types.ts:416

---

### lastMessage

• **lastMessage**: _[Message](domain.message.md) | null_

Defined in types.ts:600

---

### lastReadMessageId

• **lastReadMessageId**: _string | null_

_Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)_

Defined in types.ts:412

---

### muteUntil

• **muteUntil**: _Date | null_

Defined in types.ts:584

---

### pinnedAt

• **pinnedAt**: _Date | null_

Defined in types.ts:586

---

### serverCreated

• **serverCreated**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)_

Defined in types.ts:420

---

### serverManaged

• **serverManaged**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)_

Defined in types.ts:422

---

### startAfter

• **startAfter**: _Date | null_

Defined in types.ts:590

---

### type

• **type**: _[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)_

_Inherited from [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)_

Defined in types.ts:414

---

### updatedAt

• **updatedAt**: _Date_

Defined in types.ts:598

---

### visible

• **visible**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)_

Defined in types.ts:418
