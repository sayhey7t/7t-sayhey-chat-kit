[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [UserSelf](dto.userself.md)

# Class: UserSelf

## Hierarchy

↳ [User](dto.user.md)

↳ **UserSelf**

## Index

### Properties

- [deletedAt](dto.userself.md#markdown-header-deletedat)
- [disabled](dto.userself.md#markdown-header-disabled)
- [email](dto.userself.md#markdown-header-email)
- [firstName](dto.userself.md#markdown-header-firstname)
- [id](dto.userself.md#markdown-header-id)
- [lastName](dto.userself.md#markdown-header-lastname)
- [muteAllUntil](dto.userself.md#markdown-header-mutealluntil)
- [picture](dto.userself.md#markdown-header-picture)
- [refId](dto.userself.md#markdown-header-refid)

## Properties

### deletedAt

• **deletedAt**: _string | null_

_Inherited from [User](dto.user.md).[deletedAt](dto.user.md#markdown-header-deletedat)_

Defined in types.ts:434

---

### disabled

• **disabled**: _boolean_

_Inherited from [User](dto.user.md).[disabled](dto.user.md#markdown-header-disabled)_

Defined in types.ts:432

---

### email

• **email**: _string_

_Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)_

Defined in types.ts:194

---

### firstName

• **firstName**: _string_

_Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)_

Defined in types.ts:186

---

### id

• **id**: _string_

_Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)_

Defined in types.ts:184

---

### lastName

• **lastName**: _string_

_Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)_

Defined in types.ts:188

---

### muteAllUntil

• **muteAllUntil**: _string | null_

Defined in types.ts:437

---

### picture

• **picture**: _[Media](../modules/dto.md#markdown-header-media) | null_

_Inherited from [User](dto.user.md).[picture](dto.user.md#markdown-header-picture)_

Defined in types.ts:430

---

### refId

• **refId**: _string | null_

_Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)_

Defined in types.ts:190
