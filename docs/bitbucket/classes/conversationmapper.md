[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationMapper](conversationmapper.md)

# Class: ConversationMapper

## Hierarchy

- **ConversationMapper**

## Index

### Methods

- [toConversationConversableUsersBatchDomain](conversationmapper.md#markdown-header-static-toconversationconversableusersbatchdomain)
- [toConversationUsersBatchDomain](conversationmapper.md#markdown-header-static-toconversationusersbatchdomain)
- [toGenericConversationDomain](conversationmapper.md#markdown-header-static-togenericconversationdomain)
- [toGroupConversationInfoDomain](conversationmapper.md#markdown-header-static-togroupconversationinfodomain)
- [toIndividualConversationInfoDomain](conversationmapper.md#markdown-header-static-toindividualconversationinfodomain)

## Methods

### `Static` toConversationConversableUsersBatchDomain

▸ **toConversationConversableUsersBatchDomain**(`batch`: [ConversationConversableUsersBatch](dto.conversationconversableusersbatch.md)): _[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md)_

Defined in mapper.ts:70

**Parameters:**

| Name    | Type                                                                          |
| ------- | ----------------------------------------------------------------------------- |
| `batch` | [ConversationConversableUsersBatch](dto.conversationconversableusersbatch.md) |

**Returns:** _[ConversationConversableUsersBatch](domain.conversationconversableusersbatch.md)_

---

### `Static` toConversationUsersBatchDomain

▸ **toConversationUsersBatchDomain**(`batch`: [ConversationUsersBatch](dto.conversationusersbatch.md)): _[ConversationUsersBatch](domain.conversationusersbatch.md)_

Defined in mapper.ts:77

**Parameters:**

| Name    | Type                                                    |
| ------- | ------------------------------------------------------- |
| `batch` | [ConversationUsersBatch](dto.conversationusersbatch.md) |

**Returns:** _[ConversationUsersBatch](domain.conversationusersbatch.md)_

---

### `Static` toGenericConversationDomain

▸ **toGenericConversationDomain**(`conversation`: [GenericConversation](../modules/dto.md#markdown-header-genericconversation)): _[GenericConversation](../modules/domain.md#markdown-header-genericconversation)_

Defined in mapper.ts:96

**Parameters:**

| Name           | Type                                                                         |
| -------------- | ---------------------------------------------------------------------------- |
| `conversation` | [GenericConversation](../modules/dto.md#markdown-header-genericconversation) |

**Returns:** _[GenericConversation](../modules/domain.md#markdown-header-genericconversation)_

---

### `Static` toGroupConversationInfoDomain

▸ **toGroupConversationInfoDomain**(`info`: [GroupConversationInfo](dto.groupconversationinfo.md)): _[GroupConversationInfo](domain.groupconversationinfo.md)_

Defined in mapper.ts:84

**Parameters:**

| Name   | Type                                                  |
| ------ | ----------------------------------------------------- |
| `info` | [GroupConversationInfo](dto.groupconversationinfo.md) |

**Returns:** _[GroupConversationInfo](domain.groupconversationinfo.md)_

---

### `Static` toIndividualConversationInfoDomain

▸ **toIndividualConversationInfoDomain**(`info`: [UserConversationInfo](dto.userconversationinfo.md)): _[UserConversationInfo](domain.userconversationinfo.md)_

Defined in mapper.ts:90

**Parameters:**

| Name   | Type                                                |
| ------ | --------------------------------------------------- |
| `info` | [UserConversationInfo](dto.userconversationinfo.md) |

**Returns:** _[UserConversationInfo](domain.userconversationinfo.md)_
