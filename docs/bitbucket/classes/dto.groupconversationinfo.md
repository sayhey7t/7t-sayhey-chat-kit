[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [GroupConversationInfo](dto.groupconversationinfo.md)

# Class: GroupConversationInfo

## Hierarchy

- [ConversationInfo](dto.conversationinfo.md)

  ↳ **GroupConversationInfo**

## Index

### Properties

- [name](dto.groupconversationinfo.md#markdown-header-name)
- [picture](dto.groupconversationinfo.md#markdown-header-picture)

## Properties

### name

• **name**: _string_

_Inherited from [ConversationInfo](dto.conversationinfo.md).[name](dto.conversationinfo.md#markdown-header-name)_

Defined in types.ts:467

---

### picture

• **picture**: _[Media](../modules/dto.md#markdown-header-media) | null_

_Inherited from [ConversationInfo](dto.conversationinfo.md).[picture](dto.conversationinfo.md#markdown-header-picture)_

Defined in types.ts:465
