[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [UserConversationInfo](dto.userconversationinfo.md)

# Class: UserConversationInfo

## Hierarchy

- [ConversationInfo](dto.conversationinfo.md)

  ↳ **UserConversationInfo**

## Index

### Properties

- [email](dto.userconversationinfo.md#markdown-header-email)
- [id](dto.userconversationinfo.md#markdown-header-id)
- [name](dto.userconversationinfo.md#markdown-header-name)
- [picture](dto.userconversationinfo.md#markdown-header-picture)
- [refId](dto.userconversationinfo.md#markdown-header-refid)

## Properties

### email

• **email**: _string_

Defined in types.ts:473

---

### id

• **id**: _string_

Defined in types.ts:471

---

### name

• **name**: _string_

_Inherited from [ConversationInfo](dto.conversationinfo.md).[name](dto.conversationinfo.md#markdown-header-name)_

Defined in types.ts:467

---

### picture

• **picture**: _[Media](../modules/dto.md#markdown-header-media) | null_

_Inherited from [ConversationInfo](dto.conversationinfo.md).[picture](dto.conversationinfo.md#markdown-header-picture)_

Defined in types.ts:465

---

### refId

• **refId**: _string | null_

Defined in types.ts:475
