[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DefaultGroupProfileImageBase](defaultgroupprofileimagebase.md)

# Class: DefaultGroupProfileImageBase

## Hierarchy

- **DefaultGroupProfileImageBase**

  ↳ [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)

  ↳ [DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)

## Index

### Properties

- [filename](defaultgroupprofileimagebase.md#markdown-header-filename)
- [original](defaultgroupprofileimagebase.md#markdown-header-original)
- [thumbnail](defaultgroupprofileimagebase.md#markdown-header-thumbnail)

## Properties

### filename

• **filename**: _string_

Defined in types.ts:168

---

### original

• **original**: _string_

Defined in types.ts:170

---

### thumbnail

• **thumbnail**: _string_

Defined in types.ts:172
