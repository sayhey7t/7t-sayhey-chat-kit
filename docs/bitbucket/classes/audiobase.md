[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AudioBase](audiobase.md)

# Class: AudioBase

## Hierarchy

- [MediaBase](mediabase.md)

  ↳ **AudioBase**

## Index

### Properties

- [encoding](audiobase.md#markdown-header-encoding)
- [extension](audiobase.md#markdown-header-extension)
- [fileSize](audiobase.md#markdown-header-filesize)
- [fileType](audiobase.md#markdown-header-filetype)
- [filename](audiobase.md#markdown-header-filename)
- [imageInfo](audiobase.md#markdown-header-imageinfo)
- [mimeType](audiobase.md#markdown-header-mimetype)
- [urlPath](audiobase.md#markdown-header-urlpath)
- [videoInfo](audiobase.md#markdown-header-videoinfo)

## Properties

### encoding

• **encoding**: _string_

_Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)_

Defined in types.ts:273

---

### extension

• **extension**: _string_

_Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)_

Defined in types.ts:271

---

### fileSize

• **fileSize**: _number_

_Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)_

Defined in types.ts:277

---

### fileType

• **fileType**: _[FileType](../enums/filetype.md)_ = FileType.Audio

Defined in types.ts:300

---

### filename

• **filename**: _string_

_Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)_

Defined in types.ts:275

---

### imageInfo

• **imageInfo**: _null_ = null

Defined in types.ts:302

---

### mimeType

• **mimeType**: _string_

_Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)_

Defined in types.ts:269

---

### urlPath

• **urlPath**: _string_

_Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)_

Defined in types.ts:267

---

### videoInfo

• **videoInfo**: _object_

Defined in types.ts:304

#### Type declaration:

- **duration**: _number_

- **thumbnail**: _null_
