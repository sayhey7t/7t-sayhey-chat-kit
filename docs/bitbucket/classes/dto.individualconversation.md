[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [IndividualConversation](dto.individualconversation.md)

# Class: IndividualConversation

## Hierarchy

↳ [Conversation](dto.conversation.md)

↳ **IndividualConversation**

## Index

### Properties

- [badge](dto.individualconversation.md#markdown-header-badge)
- [createdAt](dto.individualconversation.md#markdown-header-createdat)
- [deletedFromConversationAt](dto.individualconversation.md#markdown-header-deletedfromconversationat)
- [endBefore](dto.individualconversation.md#markdown-header-endbefore)
- [group](dto.individualconversation.md#markdown-header-optional-group)
- [id](dto.individualconversation.md#markdown-header-id)
- [isOwner](dto.individualconversation.md#markdown-header-isowner)
- [lastMessage](dto.individualconversation.md#markdown-header-lastmessage)
- [lastReadMessageId](dto.individualconversation.md#markdown-header-lastreadmessageid)
- [muteUntil](dto.individualconversation.md#markdown-header-muteuntil)
- [pinnedAt](dto.individualconversation.md#markdown-header-pinnedat)
- [serverCreated](dto.individualconversation.md#markdown-header-servercreated)
- [serverManaged](dto.individualconversation.md#markdown-header-servermanaged)
- [startAfter](dto.individualconversation.md#markdown-header-startafter)
- [type](dto.individualconversation.md#markdown-header-type)
- [updatedAt](dto.individualconversation.md#markdown-header-updatedat)
- [user](dto.individualconversation.md#markdown-header-user)
- [visible](dto.individualconversation.md#markdown-header-visible)

## Properties

### badge

• **badge**: _number | null_

_Inherited from [Conversation](dto.conversation.md).[badge](dto.conversation.md#markdown-header-badge)_

Defined in types.ts:482

---

### createdAt

• **createdAt**: _string_

_Inherited from [Conversation](dto.conversation.md).[createdAt](dto.conversation.md#markdown-header-createdat)_

Defined in types.ts:490

---

### deletedFromConversationAt

• **deletedFromConversationAt**: _string | null_

_Inherited from [Conversation](dto.conversation.md).[deletedFromConversationAt](dto.conversation.md#markdown-header-deletedfromconversationat)_

Defined in types.ts:488

---

### endBefore

• **endBefore**: _string | null_

_Inherited from [Conversation](dto.conversation.md).[endBefore](dto.conversation.md#markdown-header-endbefore)_

Defined in types.ts:486

---

### `Optional` group

• **group**? : _[ConversationInfo](dto.conversationinfo.md)_

_Inherited from [Conversation](dto.conversation.md).[group](dto.conversation.md#markdown-header-optional-group)_

Defined in types.ts:496

---

### id

• **id**: _string_

_Inherited from [ConversationBase](conversationbase.md).[id](conversationbase.md#markdown-header-id)_

Defined in types.ts:410

---

### isOwner

• **isOwner**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[isOwner](conversationbase.md#markdown-header-isowner)_

Defined in types.ts:416

---

### lastMessage

• **lastMessage**: _[Message](dto.message.md) | null_

_Inherited from [Conversation](dto.conversation.md).[lastMessage](dto.conversation.md#markdown-header-lastmessage)_

Defined in types.ts:494

---

### lastReadMessageId

• **lastReadMessageId**: _string | null_

_Inherited from [ConversationBase](conversationbase.md).[lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)_

Defined in types.ts:412

---

### muteUntil

• **muteUntil**: _string | null_

_Inherited from [Conversation](dto.conversation.md).[muteUntil](dto.conversation.md#markdown-header-muteuntil)_

Defined in types.ts:478

---

### pinnedAt

• **pinnedAt**: _string | null_

_Inherited from [Conversation](dto.conversation.md).[pinnedAt](dto.conversation.md#markdown-header-pinnedat)_

Defined in types.ts:480

---

### serverCreated

• **serverCreated**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverCreated](conversationbase.md#markdown-header-servercreated)_

Defined in types.ts:420

---

### serverManaged

• **serverManaged**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[serverManaged](conversationbase.md#markdown-header-servermanaged)_

Defined in types.ts:422

---

### startAfter

• **startAfter**: _string | null_

_Inherited from [Conversation](dto.conversation.md).[startAfter](dto.conversation.md#markdown-header-startafter)_

Defined in types.ts:484

---

### type

• **type**: _[Individual](../enums/conversationtype.md#markdown-header-individual)_

_Overrides [ConversationBase](conversationbase.md).[type](conversationbase.md#markdown-header-type)_

Defined in types.ts:506

---

### updatedAt

• **updatedAt**: _string_

_Inherited from [Conversation](dto.conversation.md).[updatedAt](dto.conversation.md#markdown-header-updatedat)_

Defined in types.ts:492

---

### user

• **user**: _[UserConversationInfo](dto.userconversationinfo.md)_

_Overrides [Conversation](dto.conversation.md).[user](dto.conversation.md#markdown-header-optional-user)_

Defined in types.ts:508

---

### visible

• **visible**: _boolean_

_Inherited from [ConversationBase](conversationbase.md).[visible](conversationbase.md#markdown-header-visible)_

Defined in types.ts:418
