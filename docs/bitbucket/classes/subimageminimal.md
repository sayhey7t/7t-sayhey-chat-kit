[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [SubImageMinimal](subimageminimal.md)

# Class: SubImageMinimal

## Hierarchy

- **SubImageMinimal**

## Index

### Properties

- [height](subimageminimal.md#markdown-header-height)
- [imageSize](subimageminimal.md#markdown-header-imagesize)
- [postfix](subimageminimal.md#markdown-header-postfix)
- [width](subimageminimal.md#markdown-header-width)

## Properties

### height

• **height**: _number_

Defined in types.ts:217

---

### imageSize

• **imageSize**: _[ImageSizes](../enums/imagesizes.md)_

Defined in types.ts:213

---

### postfix

• **postfix**: _string_

Defined in types.ts:219

---

### width

• **width**: _number_

Defined in types.ts:215
