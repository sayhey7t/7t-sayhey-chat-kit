[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DocumentBase](documentbase.md)

# Class: DocumentBase

## Hierarchy

- [MediaBase](mediabase.md)

  ↳ **DocumentBase**

## Index

### Properties

- [encoding](documentbase.md#markdown-header-encoding)
- [extension](documentbase.md#markdown-header-extension)
- [fileSize](documentbase.md#markdown-header-filesize)
- [fileType](documentbase.md#markdown-header-filetype)
- [filename](documentbase.md#markdown-header-filename)
- [imageInfo](documentbase.md#markdown-header-imageinfo)
- [mimeType](documentbase.md#markdown-header-mimetype)
- [urlPath](documentbase.md#markdown-header-urlpath)
- [videoInfo](documentbase.md#markdown-header-videoinfo)

## Properties

### encoding

• **encoding**: _string_

_Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)_

Defined in types.ts:273

---

### extension

• **extension**: _string_

_Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)_

Defined in types.ts:271

---

### fileSize

• **fileSize**: _number_

_Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)_

Defined in types.ts:277

---

### fileType

• **fileType**: _[FileType](../enums/filetype.md)_ = FileType.Other

Defined in types.ts:311

---

### filename

• **filename**: _string_

_Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)_

Defined in types.ts:275

---

### imageInfo

• **imageInfo**: _null_ = null

Defined in types.ts:313

---

### mimeType

• **mimeType**: _string_

_Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)_

Defined in types.ts:269

---

### urlPath

• **urlPath**: _string_

_Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)_

Defined in types.ts:267

---

### videoInfo

• **videoInfo**: _null_ = null

Defined in types.ts:315
