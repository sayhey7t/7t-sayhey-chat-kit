[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [ConversationBase](conversationbase.md)

# Class: ConversationBase

## Hierarchy

- **ConversationBase**

  ↳ [Conversation](dto.conversation.md)

  ↳ [Conversation](domain.conversation.md)

## Index

### Properties

- [id](conversationbase.md#markdown-header-id)
- [isOwner](conversationbase.md#markdown-header-isowner)
- [lastReadMessageId](conversationbase.md#markdown-header-lastreadmessageid)
- [serverCreated](conversationbase.md#markdown-header-servercreated)
- [serverManaged](conversationbase.md#markdown-header-servermanaged)
- [type](conversationbase.md#markdown-header-type)
- [visible](conversationbase.md#markdown-header-visible)

## Properties

### id

• **id**: _string_

Defined in types.ts:410

---

### isOwner

• **isOwner**: _boolean_

Defined in types.ts:416

---

### lastReadMessageId

• **lastReadMessageId**: _string | null_

Defined in types.ts:412

---

### serverCreated

• **serverCreated**: _boolean_

Defined in types.ts:420

---

### serverManaged

• **serverManaged**: _boolean_

Defined in types.ts:422

---

### type

• **type**: _[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)_

Defined in types.ts:414

---

### visible

• **visible**: _boolean_

Defined in types.ts:418
