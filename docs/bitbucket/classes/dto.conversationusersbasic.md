[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationUsersBasic](dto.conversationusersbasic.md)

# Class: ConversationUsersBasic

## Hierarchy

- **ConversationUsersBasic**

## Index

### Properties

- [deletedFromConversationAt](dto.conversationusersbasic.md#markdown-header-deletedfromconversationat)
- [id](dto.conversationusersbasic.md#markdown-header-id)
- [isOwner](dto.conversationusersbasic.md#markdown-header-isowner)
- [userDeletedAt](dto.conversationusersbasic.md#markdown-header-userdeletedat)
- [userDisabled](dto.conversationusersbasic.md#markdown-header-userdisabled)

## Properties

### deletedFromConversationAt

• **deletedFromConversationAt**: _string | null_

Defined in types.ts:523

---

### id

• **id**: _string_

Defined in types.ts:521

---

### isOwner

• **isOwner**: _boolean_

Defined in types.ts:519

---

### userDeletedAt

• **userDeletedAt**: _string | null_

Defined in types.ts:527

---

### userDisabled

• **userDisabled**: _boolean_

Defined in types.ts:525
