[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [Message](dto.message.md)

# Class: Message

## Hierarchy

- [MessageBase](messagebase.md)

  ↳ **Message**

## Index

### Properties

- [conversationId](dto.message.md#markdown-header-conversationid)
- [createdAt](dto.message.md#markdown-header-createdat)
- [deletedAt](dto.message.md#markdown-header-deletedat)
- [file](dto.message.md#markdown-header-file)
- [id](dto.message.md#markdown-header-id)
- [reactions](dto.message.md#markdown-header-reactions)
- [refUrl](dto.message.md#markdown-header-refurl)
- [sender](dto.message.md#markdown-header-sender)
- [sent](dto.message.md#markdown-header-sent)
- [sequence](dto.message.md#markdown-header-sequence)
- [systemMessage](dto.message.md#markdown-header-systemmessage)
- [text](dto.message.md#markdown-header-text)
- [type](dto.message.md#markdown-header-type)

## Properties

### conversationId

• **conversationId**: _string_

_Inherited from [MessageBase](messagebase.md).[conversationId](messagebase.md#markdown-header-conversationid)_

Defined in types.ts:391

---

### createdAt

• **createdAt**: _string_

Defined in types.ts:452

---

### deletedAt

• **deletedAt**: _string | null_

Defined in types.ts:462

---

### file

• **file**: _[Media](../modules/dto.md#markdown-header-media) | null_

Defined in types.ts:456

---

### id

• **id**: _string_

_Inherited from [MessageBase](messagebase.md).[id](messagebase.md#markdown-header-id)_

Defined in types.ts:389

---

### reactions

• **reactions**: _[ReactionsObject](reactionsobject.md)_

_Inherited from [MessageBase](messagebase.md).[reactions](messagebase.md#markdown-header-reactions)_

Defined in types.ts:401

---

### refUrl

• **refUrl**: _string | null_

_Inherited from [MessageBase](messagebase.md).[refUrl](messagebase.md#markdown-header-refurl)_

Defined in types.ts:397

---

### sender

• **sender**: _[User](dto.user.md) | null_

Defined in types.ts:454

---

### sent

• **sent**: _boolean_

Defined in types.ts:458

---

### sequence

• **sequence**: _number_

_Inherited from [MessageBase](messagebase.md).[sequence](messagebase.md#markdown-header-sequence)_

Defined in types.ts:399

---

### systemMessage

• **systemMessage**: _[SystemMessage](dto.systemmessage.md) | null_

Defined in types.ts:460

---

### text

• **text**: _string | null_

_Inherited from [MessageBase](messagebase.md).[text](messagebase.md#markdown-header-text)_

Defined in types.ts:395

---

### type

• **type**: _[MessageType](../enums/messagetype.md)_

_Inherited from [MessageBase](messagebase.md).[type](messagebase.md#markdown-header-type)_

Defined in types.ts:393
