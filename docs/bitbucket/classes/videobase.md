[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [VideoBase](videobase.md)

# Class: VideoBase

## Hierarchy

- [MediaBase](mediabase.md)

  ↳ **VideoBase**

## Index

### Properties

- [encoding](videobase.md#markdown-header-encoding)
- [extension](videobase.md#markdown-header-extension)
- [fileSize](videobase.md#markdown-header-filesize)
- [fileType](videobase.md#markdown-header-filetype)
- [filename](videobase.md#markdown-header-filename)
- [imageInfo](videobase.md#markdown-header-imageinfo)
- [mimeType](videobase.md#markdown-header-mimetype)
- [urlPath](videobase.md#markdown-header-urlpath)
- [videoInfo](videobase.md#markdown-header-videoinfo)

## Properties

### encoding

• **encoding**: _string_

_Inherited from [MediaBase](mediabase.md).[encoding](mediabase.md#markdown-header-encoding)_

Defined in types.ts:273

---

### extension

• **extension**: _string_

_Inherited from [MediaBase](mediabase.md).[extension](mediabase.md#markdown-header-extension)_

Defined in types.ts:271

---

### fileSize

• **fileSize**: _number_

_Inherited from [MediaBase](mediabase.md).[fileSize](mediabase.md#markdown-header-filesize)_

Defined in types.ts:277

---

### fileType

• **fileType**: _[FileType](../enums/filetype.md)_ = FileType.Video

Defined in types.ts:289

---

### filename

• **filename**: _string_

_Inherited from [MediaBase](mediabase.md).[filename](mediabase.md#markdown-header-filename)_

Defined in types.ts:275

---

### imageInfo

• **imageInfo**: _null_ = null

Defined in types.ts:291

---

### mimeType

• **mimeType**: _string_

_Inherited from [MediaBase](mediabase.md).[mimeType](mediabase.md#markdown-header-mimetype)_

Defined in types.ts:269

---

### urlPath

• **urlPath**: _string_

_Inherited from [MediaBase](mediabase.md).[urlPath](mediabase.md#markdown-header-urlpath)_

Defined in types.ts:267

---

### videoInfo

• **videoInfo**: _object_

Defined in types.ts:293

#### Type declaration:

- **duration**: _number_

- **thumbnail**: _[ImageBase](imagebase.md) | null_
