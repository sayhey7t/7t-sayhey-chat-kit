[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationConversableUsersBatch](dto.conversationconversableusersbatch.md)

# Class: ConversationConversableUsersBatch

## Hierarchy

↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

↳ **ConversationConversableUsersBatch**

## Index

### Properties

- [completed](dto.conversationconversableusersbatch.md#markdown-header-completed)
- [conversationId](dto.conversationconversableusersbatch.md#markdown-header-conversationid)
- [includeRefIdInSearch](dto.conversationconversableusersbatch.md#markdown-header-includerefidinsearch)
- [limit](dto.conversationconversableusersbatch.md#markdown-header-limit)
- [nextOffset](dto.conversationconversableusersbatch.md#markdown-header-nextoffset)
- [offset](dto.conversationconversableusersbatch.md#markdown-header-offset)
- [results](dto.conversationconversableusersbatch.md#markdown-header-results)
- [search](dto.conversationconversableusersbatch.md#markdown-header-search)
- [totalCount](dto.conversationconversableusersbatch.md#markdown-header-totalcount)

## Properties

### completed

• **completed**: _boolean_

_Inherited from [Paginatable](paginatable.md).[completed](paginatable.md#markdown-header-completed)_

Defined in types.ts:241

---

### conversationId

• **conversationId**: _string_

_Inherited from [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md).[conversationId](conversationconversableusersbatchmeta.md#markdown-header-conversationid)_

Defined in types.ts:247

---

### includeRefIdInSearch

• **includeRefIdInSearch**: _boolean_

_Inherited from [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md).[includeRefIdInSearch](conversationconversableusersbatchmeta.md#markdown-header-includerefidinsearch)_

Defined in types.ts:245

---

### limit

• **limit**: _number_

_Inherited from [Paginatable](paginatable.md).[limit](paginatable.md#markdown-header-limit)_

Defined in types.ts:239

---

### nextOffset

• **nextOffset**: _number_

_Inherited from [Paginatable](paginatable.md).[nextOffset](paginatable.md#markdown-header-nextoffset)_

Defined in types.ts:237

---

### offset

• **offset**: _number_

_Inherited from [Paginatable](paginatable.md).[offset](paginatable.md#markdown-header-offset)_

Defined in types.ts:235

---

### results

• **results**: _[User](dto.user.md)[]_

Defined in types.ts:440

---

### search

• **search**: _string | null_

_Inherited from [Paginatable](paginatable.md).[search](paginatable.md#markdown-header-search)_

Defined in types.ts:231

---

### totalCount

• **totalCount**: _number_

_Inherited from [Paginatable](paginatable.md).[totalCount](paginatable.md#markdown-header-totalcount)_

Defined in types.ts:233
