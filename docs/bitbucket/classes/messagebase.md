[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [MessageBase](messagebase.md)

# Class: MessageBase

## Hierarchy

- **MessageBase**

  ↳ [Message](dto.message.md)

  ↳ [Message](domain.message.md)

## Index

### Properties

- [conversationId](messagebase.md#markdown-header-conversationid)
- [id](messagebase.md#markdown-header-id)
- [reactions](messagebase.md#markdown-header-reactions)
- [refUrl](messagebase.md#markdown-header-refurl)
- [sequence](messagebase.md#markdown-header-sequence)
- [text](messagebase.md#markdown-header-text)
- [type](messagebase.md#markdown-header-type)

## Properties

### conversationId

• **conversationId**: _string_

Defined in types.ts:391

---

### id

• **id**: _string_

Defined in types.ts:389

---

### reactions

• **reactions**: _[ReactionsObject](reactionsobject.md)_

Defined in types.ts:401

---

### refUrl

• **refUrl**: _string | null_

Defined in types.ts:397

---

### sequence

• **sequence**: _number_

Defined in types.ts:399

---

### text

• **text**: _string | null_

Defined in types.ts:395

---

### type

• **type**: _[MessageType](../enums/messagetype.md)_

Defined in types.ts:393
