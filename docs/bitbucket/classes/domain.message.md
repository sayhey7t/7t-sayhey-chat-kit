[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [Message](domain.message.md)

# Class: Message

## Hierarchy

- [MessageBase](messagebase.md)

  ↳ **Message**

## Index

### Properties

- [conversationId](domain.message.md#markdown-header-conversationid)
- [createdAt](domain.message.md#markdown-header-createdat)
- [deletedAt](domain.message.md#markdown-header-deletedat)
- [file](domain.message.md#markdown-header-file)
- [id](domain.message.md#markdown-header-id)
- [reactions](domain.message.md#markdown-header-reactions)
- [refUrl](domain.message.md#markdown-header-refurl)
- [sender](domain.message.md#markdown-header-sender)
- [sent](domain.message.md#markdown-header-sent)
- [sequence](domain.message.md#markdown-header-sequence)
- [systemMessage](domain.message.md#markdown-header-systemmessage)
- [text](domain.message.md#markdown-header-text)
- [type](domain.message.md#markdown-header-type)

## Properties

### conversationId

• **conversationId**: _string_

_Inherited from [MessageBase](messagebase.md).[conversationId](messagebase.md#markdown-header-conversationid)_

Defined in types.ts:391

---

### createdAt

• **createdAt**: _Date_

Defined in types.ts:558

---

### deletedAt

• **deletedAt**: _Date | null_

Defined in types.ts:568

---

### file

• **file**: _[Media](../modules/domain.md#markdown-header-media) | null_

Defined in types.ts:562

---

### id

• **id**: _string_

_Inherited from [MessageBase](messagebase.md).[id](messagebase.md#markdown-header-id)_

Defined in types.ts:389

---

### reactions

• **reactions**: _[ReactionsObject](reactionsobject.md)_

_Inherited from [MessageBase](messagebase.md).[reactions](messagebase.md#markdown-header-reactions)_

Defined in types.ts:401

---

### refUrl

• **refUrl**: _string | null_

_Inherited from [MessageBase](messagebase.md).[refUrl](messagebase.md#markdown-header-refurl)_

Defined in types.ts:397

---

### sender

• **sender**: _[User](domain.user.md) | null_

Defined in types.ts:560

---

### sent

• **sent**: _boolean_

Defined in types.ts:564

---

### sequence

• **sequence**: _number_

_Inherited from [MessageBase](messagebase.md).[sequence](messagebase.md#markdown-header-sequence)_

Defined in types.ts:399

---

### systemMessage

• **systemMessage**: _[SystemMessage](domain.systemmessage.md) | null_

Defined in types.ts:566

---

### text

• **text**: _string | null_

_Inherited from [MessageBase](messagebase.md).[text](messagebase.md#markdown-header-text)_

Defined in types.ts:395

---

### type

• **type**: _[MessageType](../enums/messagetype.md)_

_Inherited from [MessageBase](messagebase.md).[type](messagebase.md#markdown-header-type)_

Defined in types.ts:393
