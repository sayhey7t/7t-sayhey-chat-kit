[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [User](domain.user.md)

# Class: User

## Hierarchy

↳ [UserDetail](userdetail.md)

↳ **User**

↳ [UserSelf](domain.userself.md)

↳ [ConversationUser](domain.conversationuser.md)

## Index

### Properties

- [deletedAt](domain.user.md#markdown-header-deletedat)
- [disabled](domain.user.md#markdown-header-disabled)
- [email](domain.user.md#markdown-header-email)
- [firstName](domain.user.md#markdown-header-firstname)
- [id](domain.user.md#markdown-header-id)
- [lastName](domain.user.md#markdown-header-lastname)
- [picture](domain.user.md#markdown-header-picture)
- [refId](domain.user.md#markdown-header-refid)

## Properties

### deletedAt

• **deletedAt**: _Date | null_

Defined in types.ts:540

---

### disabled

• **disabled**: _boolean_

Defined in types.ts:538

---

### email

• **email**: _string_

_Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)_

Defined in types.ts:194

---

### firstName

• **firstName**: _string_

_Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)_

Defined in types.ts:186

---

### id

• **id**: _string_

_Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)_

Defined in types.ts:184

---

### lastName

• **lastName**: _string_

_Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)_

Defined in types.ts:188

---

### picture

• **picture**: _[Media](../modules/domain.md#markdown-header-media) | null_

Defined in types.ts:536

---

### refId

• **refId**: _string | null_

_Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)_

Defined in types.ts:190
