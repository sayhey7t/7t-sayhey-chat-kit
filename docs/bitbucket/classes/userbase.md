[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserBase](userbase.md)

# Class: UserBase

## Hierarchy

- **UserBase**

  ↳ [UserDetail](userdetail.md)

## Index

### Properties

- [firstName](userbase.md#markdown-header-firstname)
- [id](userbase.md#markdown-header-id)
- [lastName](userbase.md#markdown-header-lastname)
- [refId](userbase.md#markdown-header-refid)

## Properties

### firstName

• **firstName**: _string_

Defined in types.ts:186

---

### id

• **id**: _string_

Defined in types.ts:184

---

### lastName

• **lastName**: _string_

Defined in types.ts:188

---

### refId

• **refId**: _string | null_

Defined in types.ts:190
