[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [AppMapper](appmapper.md)

# Class: AppMapper

## Hierarchy

- **AppMapper**

## Index

### Methods

- [toAuthDomain](appmapper.md#markdown-header-static-toauthdomain)
- [toProfileImageDomainList](appmapper.md#markdown-header-static-toprofileimagedomainlist)

## Methods

### `Static` toAuthDomain

▸ **toAuthDomain**(`authDetails`: [Auth](dto.auth.md)): _[Auth](domain.auth.md)_

Defined in mapper.ts:8

**Parameters:**

| Name          | Type                |
| ------------- | ------------------- |
| `authDetails` | [Auth](dto.auth.md) |

**Returns:** _[Auth](domain.auth.md)_

---

### `Static` toProfileImageDomainList

▸ **toProfileImageDomainList**(`imageList`: [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)[]): _[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[]_

Defined in mapper.ts:4

**Parameters:**

| Name        | Type                                                          |
| ----------- | ------------------------------------------------------------- |
| `imageList` | [DefaultGroupProfileImage](dto.defaultgroupprofileimage.md)[] |

**Returns:** _[DefaultGroupProfileImage](domain.defaultgroupprofileimage.md)[]_
