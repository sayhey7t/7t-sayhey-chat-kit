[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [DTO](../modules/dto.md) › [ConversationInfo](dto.conversationinfo.md)

# Class: ConversationInfo

## Hierarchy

- **ConversationInfo**

  ↳ [GroupConversationInfo](dto.groupconversationinfo.md)

  ↳ [UserConversationInfo](dto.userconversationinfo.md)

## Index

### Properties

- [name](dto.conversationinfo.md#markdown-header-name)
- [picture](dto.conversationinfo.md#markdown-header-picture)

## Properties

### name

• **name**: _string_

Defined in types.ts:467

---

### picture

• **picture**: _[Media](../modules/dto.md#markdown-header-media) | null_

Defined in types.ts:465
