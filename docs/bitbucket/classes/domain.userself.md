[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](../modules/domain.md) › [UserSelf](domain.userself.md)

# Class: UserSelf

## Hierarchy

↳ [User](domain.user.md)

↳ **UserSelf**

## Index

### Properties

- [deletedAt](domain.userself.md#markdown-header-deletedat)
- [disabled](domain.userself.md#markdown-header-disabled)
- [email](domain.userself.md#markdown-header-email)
- [firstName](domain.userself.md#markdown-header-firstname)
- [id](domain.userself.md#markdown-header-id)
- [lastName](domain.userself.md#markdown-header-lastname)
- [muteAllUntil](domain.userself.md#markdown-header-mutealluntil)
- [picture](domain.userself.md#markdown-header-picture)
- [refId](domain.userself.md#markdown-header-refid)

## Properties

### deletedAt

• **deletedAt**: _Date | null_

_Inherited from [User](domain.user.md).[deletedAt](domain.user.md#markdown-header-deletedat)_

Defined in types.ts:540

---

### disabled

• **disabled**: _boolean_

_Inherited from [User](domain.user.md).[disabled](domain.user.md#markdown-header-disabled)_

Defined in types.ts:538

---

### email

• **email**: _string_

_Inherited from [UserDetail](userdetail.md).[email](userdetail.md#markdown-header-email)_

Defined in types.ts:194

---

### firstName

• **firstName**: _string_

_Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)_

Defined in types.ts:186

---

### id

• **id**: _string_

_Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)_

Defined in types.ts:184

---

### lastName

• **lastName**: _string_

_Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)_

Defined in types.ts:188

---

### muteAllUntil

• **muteAllUntil**: _Date | null_

Defined in types.ts:543

---

### picture

• **picture**: _[Media](../modules/domain.md#markdown-header-media) | null_

_Inherited from [User](domain.user.md).[picture](domain.user.md#markdown-header-picture)_

Defined in types.ts:536

---

### refId

• **refId**: _string | null_

_Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)_

Defined in types.ts:190
