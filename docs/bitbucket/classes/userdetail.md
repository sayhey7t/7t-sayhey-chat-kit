[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [UserDetail](userdetail.md)

# Class: UserDetail

## Hierarchy

- [UserBase](userbase.md)

  ↳ **UserDetail**

  ↳ [User](dto.user.md)

  ↳ [User](domain.user.md)

## Index

### Properties

- [email](userdetail.md#markdown-header-email)
- [firstName](userdetail.md#markdown-header-firstname)
- [id](userdetail.md#markdown-header-id)
- [lastName](userdetail.md#markdown-header-lastname)
- [refId](userdetail.md#markdown-header-refid)

## Properties

### email

• **email**: _string_

Defined in types.ts:194

---

### firstName

• **firstName**: _string_

_Inherited from [UserBase](userbase.md).[firstName](userbase.md#markdown-header-firstname)_

Defined in types.ts:186

---

### id

• **id**: _string_

_Inherited from [UserBase](userbase.md).[id](userbase.md#markdown-header-id)_

Defined in types.ts:184

---

### lastName

• **lastName**: _string_

_Inherited from [UserBase](userbase.md).[lastName](userbase.md#markdown-header-lastname)_

Defined in types.ts:188

---

### refId

• **refId**: _string | null_

_Inherited from [UserBase](userbase.md).[refId](userbase.md#markdown-header-refid)_

Defined in types.ts:190
