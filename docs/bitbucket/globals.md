[@7t/sayhey-chat-kit](README.md) › [Globals](globals.md)

# @7t/sayhey-chat-kit

## Index

### Namespaces

- [DTO](modules/dto.md)
- [Domain](modules/domain.md)
- [ErrorType](modules/errortype.md)

### Enumerations

- [ConversationType](enums/conversationtype.md)
- [ConversationUserQueryType](enums/conversationuserquerytype.md)
- [FileType](enums/filetype.md)
- [HttpMethod](enums/httpmethod.md)
- [ImageSizes](enums/imagesizes.md)
- [MessageType](enums/messagetype.md)
- [ReactionType](enums/reactiontype.md)
- [SystemMessageType](enums/systemmessagetype.md)

### Classes

- [AppError](classes/apperror.md)
- [AppMapper](classes/appmapper.md)
- [AudioBase](classes/audiobase.md)
- [AuthBase](classes/authbase.md)
- [ChatKitServer](classes/chatkitserver.md)
- [ConversationBase](classes/conversationbase.md)
- [ConversationConversableUsersBatchMeta](classes/conversationconversableusersbatchmeta.md)
- [ConversationMapper](classes/conversationmapper.md)
- [ConversationUsersBatchBase](classes/conversationusersbatchbase.md)
- [ConversationUsersBatchMeta](classes/conversationusersbatchmeta.md)
- [DefaultGroupProfileImageBase](classes/defaultgroupprofileimagebase.md)
- [DocumentBase](classes/documentbase.md)
- [ImageBase](classes/imagebase.md)
- [ImageInfoMedia](classes/imageinfomedia.md)
- [MediaBase](classes/mediabase.md)
- [MediaMapper](classes/mediamapper.md)
- [MessageBase](classes/messagebase.md)
- [MessageMapper](classes/messagemapper.md)
- [Paginatable](classes/paginatable.md)
- [ReactionsObject](classes/reactionsobject.md)
- [SubImageMinimal](classes/subimageminimal.md)
- [SystemMessageBase](classes/systemmessagebase.md)
- [UserBase](classes/userbase.md)
- [UserDetail](classes/userdetail.md)
- [UserMapper](classes/usermapper.md)
- [UserSimpleInfo](classes/usersimpleinfo.md)
- [VideoBase](classes/videobase.md)

### Type aliases

- [FileBase](globals.md#markdown-header-filebase)

### Variables

- [API_KEY_NAME](globals.md#markdown-header-const-api_key_name)
- [API_VERSION](globals.md#markdown-header-const-api_version)

### Functions

- [checkApiKey](globals.md#markdown-header-checkapikey)
- [requestFailed](globals.md#markdown-header-requestfailed)

### Object literals

- [Routes](globals.md#markdown-header-const-routes)

## Type aliases

### FileBase

Ƭ **FileBase**: _[ImageBase](classes/imagebase.md) | [VideoBase](classes/videobase.md) | [AudioBase](classes/audiobase.md) | [DocumentBase](classes/documentbase.md)_

Defined in types.ts:318

## Variables

### `Const` API_KEY_NAME

• **API_KEY_NAME**: _"x-sayhey-server-api-key"_ = "x-sayhey-server-api-key"

Defined in constants.ts:3

---

### `Const` API_VERSION

• **API_VERSION**: _"v1"_ = "v1"

Defined in constants.ts:4

## Functions

### checkApiKey

▸ **checkApiKey**(`config`: AxiosRequestConfig): _AxiosRequestConfig_

Defined in axios.ts:6

**Parameters:**

| Name     | Type               |
| -------- | ------------------ |
| `config` | AxiosRequestConfig |

**Returns:** _AxiosRequestConfig_

---

### requestFailed

▸ **requestFailed**(`error`: AxiosError): _void_

Defined in axios.ts:15

**Parameters:**

| Name    | Type       |
| ------- | ---------- |
| `error` | AxiosError |

**Returns:** _void_

## Object literals

### `Const` Routes

### ▪ **Routes**: _object_

Defined in constants.ts:6

▪ **AddUsersToConversation**: _object_

Defined in constants.ts:56

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

- **Endpoint**(`conversationId`: string): _string_

▪ **CreateGroupWithOwner**: _object_

Defined in constants.ts:78

- **Endpoint**: _string_ = "/conversations/group-with-owner"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

▪ **DeleteAllUsersInConversation**: _object_

Defined in constants.ts:74

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

- **Endpoint**(`conversationId`: string): _string_

▪ **DeleteMessage**: _object_

Defined in constants.ts:110

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Delete

- **Endpoint**(`messageId`: string): _string_

▪ **DeleteUser**: _object_

Defined in constants.ts:114

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Delete

- **Endpoint**(`userId`: string): _string_

▪ **DisableUser**: _object_

Defined in constants.ts:98

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Put

- **Endpoint**(`userId`: string): _string_

▪ **EnableUser**: _object_

Defined in constants.ts:102

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Delete

- **Endpoint**(`userId`: string): _string_

▪ **GetConversationBasicUsers**: _object_

Defined in constants.ts:94

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`conversationId`: string): _string_

▪ **GetConversationConversableUsers**: _object_

Defined in constants.ts:48

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`conversationId`: string): _string_

▪ **GetConversationGroupImage**: _object_

Defined in constants.ts:118

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`conversationId`: string): _string_

▪ **GetConversationUsers**: _object_

Defined in constants.ts:52

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`conversationId`: string): _string_

▪ **GetDefaultProfileImagesForGroup**: _object_

Defined in constants.ts:7

- **Endpoint**: _string_ = "/default-group-images"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

▪ **GetFileMessage**: _object_

Defined in constants.ts:122

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`messageId`: string, `fileLocalIdName`: string): _string_

▪ **GetMessages**: _object_

Defined in constants.ts:106

- **Endpoint**: _string_ = "/messages"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

▪ **GetUserProfile**: _object_

Defined in constants.ts:28

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`userId`: string): _string_

▪ **GetVideoThumbnail**: _object_

Defined in constants.ts:127

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`messageId`: string, `fileLocalIdName`: string): _string_

▪ **MakeConversationOwner**: _object_

Defined in constants.ts:64

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Put

- **Endpoint**(`conversationId`: string, `userId`: string): _string_

▪ **RemoveConversationOwner**: _object_

Defined in constants.ts:69

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Delete

- **Endpoint**(`conversationId`: string, `userId`: string): _string_

▪ **RemoveGroupImage**: _object_

Defined in constants.ts:90

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Delete

- **Endpoint**(`conversationId`: string): _string_

▪ **RemoveUserProfileImage**: _object_

Defined in constants.ts:40

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Delete

- **Endpoint**(`userId`: string): _string_

▪ **RemoveUsersFromConversation**: _object_

Defined in constants.ts:60

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

- **Endpoint**(`conversationId`: string): _string_

▪ **SignInUserWithEmail**: _object_

Defined in constants.ts:20

- **Endpoint**: _string_ = "/users/server-sign-in"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

▪ **SignInUserWithId**: _object_

Defined in constants.ts:24

- **Endpoint**: _string_ = "/users/server-sign-in-id"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

▪ **SignUpUser**: _object_

Defined in constants.ts:16

- **Endpoint**: _string_ = "/users/server-sign-up"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

▪ **UpdateGroupImage**: _object_

Defined in constants.ts:86

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Put

- **Endpoint**(`conversationId`: string): _string_

▪ **UpdateGroupInfo**: _object_

Defined in constants.ts:82

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Put

- **Endpoint**(`conversationId`: string): _string_

▪ **UpdateUserProfileImage**: _object_

Defined in constants.ts:36

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Put

- **Endpoint**(`userId`: string): _string_

▪ **UpdateUserProfileInfo**: _object_

Defined in constants.ts:32

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Put

- **Endpoint**(`userId`: string): _string_

▪ **UploadFile**: _object_

Defined in constants.ts:44

- **Endpoint**: _string_ = "/files"

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Post

▪ **ViewDefaultProfileImageForGroup**: _object_

Defined in constants.ts:11

- **Method**: _[HttpMethod](enums/httpmethod.md)_ = HttpMethod.Get

- **Endpoint**(`filename`: string, `queryString`: string): _string_
