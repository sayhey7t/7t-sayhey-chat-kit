[@7t/sayhey-chat-kit](../README.md) › [Globals](../globals.md) › [Domain](domain.md)

# Namespace: Domain

## Index

### Classes

- [Auth](../classes/domain.auth.md)
- [Conversation](../classes/domain.conversation.md)
- [ConversationConversableUsersBatch](../classes/domain.conversationconversableusersbatch.md)
- [ConversationInfo](../classes/domain.conversationinfo.md)
- [ConversationUser](../classes/domain.conversationuser.md)
- [ConversationUsersBasic](../classes/domain.conversationusersbasic.md)
- [ConversationUsersBatch](../classes/domain.conversationusersbatch.md)
- [DefaultGroupProfileImage](../classes/domain.defaultgroupprofileimage.md)
- [GroupConversation](../classes/domain.groupconversation.md)
- [GroupConversationInfo](../classes/domain.groupconversationinfo.md)
- [IndividualConversation](../classes/domain.individualconversation.md)
- [Message](../classes/domain.message.md)
- [MessageDeleted](../classes/domain.messagedeleted.md)
- [SystemMessage](../classes/domain.systemmessage.md)
- [User](../classes/domain.user.md)
- [UserConversationInfo](../classes/domain.userconversationinfo.md)
- [UserSelf](../classes/domain.userself.md)

### Type aliases

- [GenericConversation](domain.md#markdown-header-genericconversation)
- [Media](domain.md#markdown-header-media)

## Type aliases

### GenericConversation

Ƭ **GenericConversation**: _[GroupConversation](../classes/domain.groupconversation.md) | [IndividualConversation](../classes/domain.individualconversation.md)_

Defined in types.ts:612

---

### Media

Ƭ **Media**: _[MediaBase](../classes/mediabase.md)_

Defined in types.ts:534
