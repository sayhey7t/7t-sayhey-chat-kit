'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.ConversationMapper = exports.MessageMapper = exports.MediaMapper = exports.UserMapper = exports.AppMapper = void 0
const types_1 = require('./types')
class AppMapper {}
exports.AppMapper = AppMapper
AppMapper.toProfileImageDomainList = imageList => [...imageList]
AppMapper.toAuthDomain = authDetails => Object.assign({}, authDetails)
class UserMapper {}
exports.UserMapper = UserMapper
UserMapper.toUserDomain = user =>
  Object.assign(Object.assign({}, user), {
    deletedAt: user.deletedAt ? new Date(user.deletedAt) : null,
    picture: user.picture ? MediaMapper.toMediaDomain(user.picture) : null
  })
UserMapper.toConversationUserDomain = user =>
  Object.assign(Object.assign(Object.assign({}, user), UserMapper.toUserDomain(user)), {
    deletedFromConversationAt: user.deletedFromConversationAt
      ? new Date(user.deletedFromConversationAt)
      : null
  })
UserMapper.toUserSelfDomain = user =>
  Object.assign(Object.assign({}, UserMapper.toUserDomain(user)), {
    muteAllUntil: user.muteAllUntil ? new Date(user.muteAllUntil) : null
  })
UserMapper.toConversationUserBasicDomain = user => {
  const { deletedFromConversationAt, userDeletedAt } = user
  return Object.assign(Object.assign({}, user), {
    deletedFromConversationAt: deletedFromConversationAt
      ? new Date(deletedFromConversationAt)
      : null,
    userDeletedAt: userDeletedAt ? new Date(userDeletedAt) : null
  })
}
class MediaMapper {}
exports.MediaMapper = MediaMapper
MediaMapper.toMediaDomain = media => Object.assign({}, media)
class MessageMapper {}
exports.MessageMapper = MessageMapper
MessageMapper.toMessageDomain = message => {
  const { createdAt, sender, file, deletedAt } = message
  return Object.assign(Object.assign({}, message), {
    createdAt: new Date(createdAt),
    sender: sender ? UserMapper.toUserDomain(sender) : null,
    file: file ? MediaMapper.toMediaDomain(file) : null,
    deletedAt: deletedAt ? new Date(deletedAt) : null
  })
}
MessageMapper.toMessageDeletedDomain = messageEvent =>
  Object.assign(Object.assign({}, messageEvent), { deletedAt: new Date(messageEvent.deletedAt) })
class ConversationMapper {}
exports.ConversationMapper = ConversationMapper
ConversationMapper.toConversationConversableUsersBatchDomain = batch =>
  Object.assign(Object.assign({}, batch), { results: batch.results.map(UserMapper.toUserDomain) })
ConversationMapper.toConversationUsersBatchDomain = batch =>
  Object.assign(Object.assign({}, batch), {
    results: batch.results.map(UserMapper.toConversationUserDomain)
  })
ConversationMapper.toGroupConversationInfoDomain = info => Object.assign({}, info)
ConversationMapper.toIndividualConversationInfoDomain = info => Object.assign({}, info)
ConversationMapper.toGenericConversationDomain = conversation => {
  const {
    muteUntil,
    endBefore,
    deletedFromConversationAt,
    createdAt,
    updatedAt,
    pinnedAt,
    startAfter,
    lastMessage
  } = conversation
  const common = {
    muteUntil: muteUntil ? new Date(muteUntil) : null,
    endBefore: endBefore ? new Date(endBefore) : null,
    deletedFromConversationAt: deletedFromConversationAt
      ? new Date(deletedFromConversationAt)
      : null,
    createdAt: new Date(createdAt),
    updatedAt: new Date(updatedAt),
    pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
    startAfter: startAfter ? new Date(startAfter) : null,
    lastMessage: lastMessage ? MessageMapper.toMessageDomain(lastMessage) : null
  }
  if (conversation instanceof types_1.DTO.IndividualConversation) {
    return Object.assign(Object.assign(Object.assign({}, conversation), common), {
      user: ConversationMapper.toIndividualConversationInfoDomain(conversation.user)
    })
  }
  return Object.assign(Object.assign(Object.assign({}, conversation), common), {
    group: ConversationMapper.toGroupConversationInfoDomain(conversation.group)
  })
}
//# sourceMappingURL=mapper.js.map
