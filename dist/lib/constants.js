'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.Routes = exports.API_VERSION = exports.API_KEY_NAME = void 0
const types_1 = require('./types')
exports.API_KEY_NAME = 'x-sayhey-server-api-key'
exports.API_VERSION = 'v1'
exports.Routes = {
  GetDefaultProfileImagesForGroup: {
    Endpoint: '/default-group-images',
    Method: types_1.HttpMethod.Get
  },
  ViewDefaultProfileImageForGroup: {
    Endpoint: (filename, queryString) => `/default-group-images/${filename}${queryString}`,
    Method: types_1.HttpMethod.Get
  },
  SignUpUser: {
    Endpoint: '/users/server-sign-up',
    Method: types_1.HttpMethod.Post
  },
  SignInUserWithEmail: {
    Endpoint: '/users/server-sign-in',
    Method: types_1.HttpMethod.Post
  },
  SignInUserWithId: {
    Endpoint: '/users/server-sign-in-id',
    Method: types_1.HttpMethod.Post
  },
  GetUserProfile: {
    Endpoint: userId => `users/${userId}/profile-info`,
    Method: types_1.HttpMethod.Get
  },
  UpdateUserProfileInfo: {
    Endpoint: userId => `users/${userId}/profile-info`,
    Method: types_1.HttpMethod.Put
  },
  UpdateUserProfileImage: {
    Endpoint: userId => `users/${userId}/profile-image`,
    Method: types_1.HttpMethod.Put
  },
  RemoveUserProfileImage: {
    Endpoint: userId => `users/${userId}/profile-image`,
    Method: types_1.HttpMethod.Delete
  },
  UploadFile: {
    Endpoint: '/files',
    Method: types_1.HttpMethod.Post
  },
  GetConversationConversableUsers: {
    Endpoint: conversationId => `conversations/${conversationId}/conversable-users`,
    Method: types_1.HttpMethod.Get
  },
  GetConversationUsers: {
    Endpoint: conversationId => `conversations/${conversationId}/users`,
    Method: types_1.HttpMethod.Get
  },
  AddUsersToConversation: {
    Endpoint: conversationId => `/conversations/${conversationId}/users`,
    Method: types_1.HttpMethod.Post
  },
  RemoveUsersFromConversation: {
    Endpoint: conversationId => `/conversations/${conversationId}/delete-users`,
    Method: types_1.HttpMethod.Post
  },
  MakeConversationOwner: {
    Endpoint: (conversationId, userId) => `conversations/${conversationId}/owners/${userId}`,
    Method: types_1.HttpMethod.Put
  },
  RemoveConversationOwner: {
    Endpoint: (conversationId, userId) => `conversations/${conversationId}/owners/${userId}`,
    Method: types_1.HttpMethod.Delete
  },
  DeleteAllUsersInConversation: {
    Endpoint: conversationId => `/conversations/${conversationId}/delete-all-users`,
    Method: types_1.HttpMethod.Post
  },
  CreateGroupWithOwner: {
    Endpoint: '/conversations/group-with-owner',
    Method: types_1.HttpMethod.Post
  },
  UpdateGroupInfo: {
    Endpoint: conversationId => `conversations/${conversationId}/group-info`,
    Method: types_1.HttpMethod.Put
  },
  UpdateGroupImage: {
    Endpoint: conversationId => `/conversations/${conversationId}/group-image`,
    Method: types_1.HttpMethod.Put
  },
  RemoveGroupImage: {
    Endpoint: conversationId => `/conversations/${conversationId}/group-image`,
    Method: types_1.HttpMethod.Delete
  },
  GetConversationBasicUsers: {
    Endpoint: conversationId => `/conversations/${conversationId}/basic-users`,
    Method: types_1.HttpMethod.Get
  },
  DisableUser: {
    Endpoint: userId => `/users/${userId}/disabled`,
    Method: types_1.HttpMethod.Put
  },
  EnableUser: {
    Endpoint: userId => `/users/${userId}/disabled`,
    Method: types_1.HttpMethod.Delete
  },
  GetMessages: {
    Endpoint: '/messages',
    Method: types_1.HttpMethod.Get
  },
  DeleteMessage: {
    Endpoint: messageId => `/messages/${messageId}`,
    Method: types_1.HttpMethod.Delete
  },
  DeleteUser: {
    Endpoint: userId => `/users/${userId}`,
    Method: types_1.HttpMethod.Delete
  },
  GetConversationGroupImage: {
    Endpoint: conversationId => `/conversations/${conversationId}/group-image`,
    Method: types_1.HttpMethod.Get
  },
  GetFileMessage: {
    Endpoint: (messageId, fileLocalIdName) => `/messages/${messageId}/files/${fileLocalIdName}`,
    Method: types_1.HttpMethod.Get
  },
  GetVideoThumbnail: {
    Endpoint: (messageId, fileLocalIdName) =>
      `/messages/${messageId}/files/video-thumbnails/${fileLocalIdName}`,
    Method: types_1.HttpMethod.Get
  }
}
//# sourceMappingURL=constants.js.map
