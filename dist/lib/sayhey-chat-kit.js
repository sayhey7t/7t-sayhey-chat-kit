'use strict'
var __awaiter =
  (this && this.__awaiter) ||
  function(thisArg, _arguments, P, generator) {
    function adopt(value) {
      return value instanceof P
        ? value
        : new P(function(resolve) {
            resolve(value)
          })
    }
    return new (P || (P = Promise))(function(resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value))
        } catch (e) {
          reject(e)
        }
      }
      function rejected(value) {
        try {
          step(generator['throw'](value))
        } catch (e) {
          reject(e)
        }
      }
      function step(result) {
        result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected)
      }
      step((generator = generator.apply(thisArg, _arguments || [])).next())
    })
  }
var __rest =
  (this && this.__rest) ||
  function(s, e) {
    var t = {}
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p]
    if (s != null && typeof Object.getOwnPropertySymbols === 'function')
      for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
          t[p[i]] = s[p[i]]
      }
    return t
  }
Object.defineProperty(exports, '__esModule', { value: true })
const neverthrow_1 = require('neverthrow')
const axios_1 = require('./axios')
const constants_1 = require('./constants')
const mapper_1 = require('./mapper')
const types_1 = require('./types')
class ChatKitServer {
  static guard(cb) {
    return __awaiter(this, void 0, void 0, function*() {
      if (!this._initialized) {
        return (0, neverthrow_1.err)(
          new types_1.AppError(
            'ChatKit has not been initialized!',
            types_1.ErrorType.InternalErrorType.Uninitialized
          )
        )
      }
      try {
        const res = yield cb()
        if (res instanceof types_1.AppError) {
          return (0, neverthrow_1.err)(res)
        }
        return (0, neverthrow_1.ok)(res)
      } catch (e) {
        if (e instanceof types_1.AppError) {
          return (0, neverthrow_1.err)(e)
        }
        return (0,
        neverthrow_1.err)(new types_1.AppError('Unknown error', types_1.ErrorType.InternalErrorType.Unknown))
      }
    })
  }
  // private constructor() {
  //   // do nothing.
  // }
  static initialize(options) {
    const { sayheyApiKey, sayheyAppId, apiVersion, sayheyUrl } = options
    ChatKitServer.apiKey = sayheyApiKey
    ChatKitServer.appId = sayheyAppId
    ChatKitServer.apiVersion = apiVersion || constants_1.API_VERSION
    ChatKitServer.httpClient = (0, axios_1.default)({
      baseURL: `${sayheyUrl}/${ChatKitServer.apiVersion}/apps/${ChatKitServer.appId}`,
      apiKey: ChatKitServer.apiKey
    })
    this._initialized = true
  }
  static getFile(url, method) {
    return __awaiter(this, void 0, void 0, function*() {
      const { data } = yield ChatKitServer.httpClient({
        url,
        method,
        responseType: 'blob'
      })
      return data
    })
  }
  static streamFile(url, method) {
    return __awaiter(this, void 0, void 0, function*() {
      const { data } = yield ChatKitServer.httpClient({
        url,
        method,
        responseType: 'stream'
      })
      return data
    })
  }
  static uploadFile(file, filename) {
    return __awaiter(this, void 0, void 0, function*() {
      const {
        UploadFile: { Endpoint, Method }
      } = constants_1.Routes
      const body = new FormData()
      body.append('file', file, filename)
      const { data } = yield ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        data: body
      })
      return data.id
    })
  }
  static uploadFileWithStream(readableStream) {
    return __awaiter(this, void 0, void 0, function*() {
      const {
        UploadFile: { Endpoint, Method }
      } = constants_1.Routes
      const body = new FormData()
      body.append('file', readableStream)
      const { data } = yield ChatKitServer.httpClient({
        url: Endpoint,
        method: Method,
        data: body
      })
      return data.id
    })
  }
  /**
   * App APIs
   */
  static getDefaultProfileImagesForGroup() {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function getDefaultProfileImagesForGroup() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetDefaultProfileImagesForGroup: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint,
            method: Method
          })
          return mapper_1.AppMapper.toProfileImageDomainList(data)
        })
      })
    })
  }
  static viewDefaultProfileImageForGroup(options) {
    return __awaiter(this, void 0, void 0, function*() {
      const { filename, original, stream } = options
      return ChatKitServer.guard(function viewDefaultProfileImageForGroup() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            ViewDefaultProfileImageForGroup: { Endpoint, Method }
          } = constants_1.Routes
          const url = Endpoint(filename, !original ? '?size=thumbnail' : '')
          if (stream) {
            const data = yield ChatKitServer.streamFile(url, Method)
            return data
          }
          const data = yield ChatKitServer.getFile(url, Method)
          return data
        })
      })
    })
  }
  /**
   * User APIs
   */
  static signUpUser(params) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function signUpUser() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            SignUpUser: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint,
            method: Method,
            data: params
          })
          return mapper_1.AppMapper.toAuthDomain(data)
        })
      })
    })
  }
  static signInUserWithEmail(email) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function signInUserWithEmail() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            SignInUserWithEmail: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint,
            method: Method,
            data: {
              email
            }
          })
          return mapper_1.AppMapper.toAuthDomain(data)
        })
      })
    })
  }
  static signInUserWithId(id) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function signInUserWithId() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            SignInUserWithId: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint,
            method: Method,
            data: {
              id
            }
          })
          return mapper_1.AppMapper.toAuthDomain(data)
        })
      })
    })
  }
  static getUserProfile(userId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function getUserProfile() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetUserProfile: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method
          })
          return mapper_1.UserMapper.toUserSelfDomain(data)
        })
      })
    })
  }
  static updateUserProfileInfo(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { userId } = params,
        body = __rest(params, ['userId'])
      return ChatKitServer.guard(function updateUserProfileInfo() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            UpdateUserProfileInfo: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method,
            data: body
          })
          return true
        })
      })
    })
  }
  static updateUserProfileImage(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { userId } = params,
        body = __rest(params, ['userId'])
      return ChatKitServer.guard(function updateUserProfileImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            UpdateUserProfileImage: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method,
            data: body
          })
          return true
        })
      })
    })
  }
  static removeUserProfileImage(userId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function removeUserProfileImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            UpdateUserProfileImage: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method
          })
          return true
        })
      })
    })
  }
  static disableUser(userId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function disableUser() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            DisableUser: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method
          })
          return true
        })
      })
    })
  }
  static enableUser(userId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function enableUser() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            EnableUser: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method
          })
          return true
        })
      })
    })
  }
  static deleteUser(userId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function deleteUser() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            DeleteUser: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(userId),
            method: Method
          })
          return true
        })
      })
    })
  }
  /**
   * Conversation APIs
   */
  static getConversationConversableUsers(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId } = params,
        queryString = __rest(params, ['conversationId'])
      return ChatKitServer.guard(function getConversationConversableUsers() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetConversationConversableUsers: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            params: queryString
          })
          return mapper_1.ConversationMapper.toConversationConversableUsersBatchDomain(data)
        })
      })
    })
  }
  static getConversationUsers(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId, type = 'current' } = params,
        otherParams = __rest(params, ['conversationId', 'type'])
      return ChatKitServer.guard(function getConversationUsers() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetConversationUsers: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            params: Object.assign(Object.assign({}, otherParams), { type })
          })
          return mapper_1.ConversationMapper.toConversationUsersBatchDomain(data)
        })
      })
    })
  }
  static getConversationBasicUsers(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId, type = 'current' } = params
      return ChatKitServer.guard(function getConversationUsers() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetConversationBasicUsers: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            params: {
              type
            }
          })
          return data.map(mapper_1.UserMapper.toConversationUserBasicDomain)
        })
      })
    })
  }
  static addUsersToConversation(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId, force, memberIds } = params
      return ChatKitServer.guard(function addUsersToConversation() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            AddUsersToConversation: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            data: {
              memberIds
            },
            params: {
              force
            }
          })
          return true
        })
      })
    })
  }
  static removeUsersFromConversation(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId, force, memberIds } = params
      return ChatKitServer.guard(function removeUsersToConversation() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            RemoveUsersFromConversation: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            data: {
              memberIds
            },
            params: {
              force
            }
          })
          return true
        })
      })
    })
  }
  static makeConversationOwner(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId, userId } = params
      return ChatKitServer.guard(function makeConversationOwner() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            MakeConversationOwner: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId, userId),
            method: Method
          })
          return true
        })
      })
    })
  }
  static removeConversationOwner(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId, userId } = params
      return ChatKitServer.guard(function removeConversationOwner() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            RemoveConversationOwner: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId, userId),
            method: Method
          })
          return true
        })
      })
    })
  }
  static deleteAllUsersInConversation(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId } = params
      return ChatKitServer.guard(function deleteAllUsersInConversation() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            DeleteAllUsersInConversation: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method
          })
          return true
        })
      })
    })
  }
  static createGroupWithOwner(params) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function removeConversationOwner() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            CreateGroupWithOwner: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint,
            method: Method,
            data: params
          })
          return mapper_1.ConversationMapper.toGenericConversationDomain(data)
        })
      })
    })
  }
  static updateGroupInfo(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId } = params,
        body = __rest(params, ['conversationId'])
      return ChatKitServer.guard(function updateGroupInfo() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            UpdateGroupInfo: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            data: body
          })
          return true
        })
      })
    })
  }
  static updateGroupImage(params) {
    return __awaiter(this, void 0, void 0, function*() {
      const { conversationId } = params,
        body = __rest(params, ['conversationId'])
      return ChatKitServer.guard(function updateGroupImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            UpdateGroupImage: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method,
            data: body
          })
          return true
        })
      })
    })
  }
  static removeGroupImage(conversationId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function removeGroupImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            RemoveGroupImage: { Endpoint, Method }
          } = constants_1.Routes
          yield ChatKitServer.httpClient({
            url: Endpoint(conversationId),
            method: Method
          })
          return true
        })
      })
    })
  }
  /**
   * Message APIs
   */
  static getMessages(params) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function getMessages() {
        var _a, _b
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetMessages: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint,
            method: Method,
            params: Object.assign(Object.assign({}, params), {
              fromDate:
                (_a = params.fromDate) === null || _a === void 0 ? void 0 : _a.toISOString(),
              toDate: (_b = params.toDate) === null || _b === void 0 ? void 0 : _b.toISOString()
            })
          })
          return data.map(mapper_1.MessageMapper.toMessageDomain)
        })
      })
    })
  }
  static deleteMessage(messageId) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function deleteMessage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            DeleteMessage: { Endpoint, Method }
          } = constants_1.Routes
          const { data } = yield ChatKitServer.httpClient({
            url: Endpoint(messageId),
            method: Method
          })
          return mapper_1.MessageMapper.toMessageDeletedDomain(data)
        })
      })
    })
  }
  /**
   * Media APIs
   */
  static getGroupProfileImage(options) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function getGroupProfileImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetConversationGroupImage: { Endpoint, Method }
          } = constants_1.Routes
          const url = Endpoint(options.conversationId)
          if (options.stream) {
            const data = yield ChatKitServer.streamFile(url, Method)
            return data
          }
          const { data } = yield ChatKitServer.httpClient({
            url,
            method: Method,
            params: {
              noRedirect: true
            }
          })
          return data.url
        })
      })
    })
  }
  static getFileMessage(options) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function getGroupProfileImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetFileMessage: { Endpoint, Method }
          } = constants_1.Routes
          const url = Endpoint(options.messageId, options.fileLocalIdName)
          if (options.stream) {
            const data = yield ChatKitServer.streamFile(url, Method)
            return data
          }
          const { data } = yield ChatKitServer.httpClient({
            url,
            method: Method,
            params: {
              noRedirect: true
            }
          })
          return data.url
        })
      })
    })
  }
  static getVideoThumbnail(options) {
    return __awaiter(this, void 0, void 0, function*() {
      return ChatKitServer.guard(function getGroupProfileImage() {
        return __awaiter(this, void 0, void 0, function*() {
          const {
            GetFileMessage: { Endpoint, Method }
          } = constants_1.Routes
          const url = Endpoint(options.messageId, options.fileLocalIdName)
          if (options.stream) {
            const data = yield ChatKitServer.streamFile(url, Method)
            return data
          }
          const { data } = yield ChatKitServer.httpClient({
            url,
            method: Method,
            params: {
              noRedirect: true
            }
          })
          return data.url
        })
      })
    })
  }
}
exports.default = ChatKitServer
ChatKitServer.apiVersion = constants_1.API_VERSION
ChatKitServer._initialized = false
//# sourceMappingURL=sayhey-chat-kit.js.map
