'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
const axios_1 = require('axios')
const constants_1 = require('./constants')
const types_1 = require('./types')
function checkApiKey(config) {
  //Safe - Configuration parameters , not an user input
  // eslint-disable-next-line security/detect-object-injection
  if (!config.headers[constants_1.API_KEY_NAME]) {
    throw new types_1.AppError(
      'Missing api key in request header',
      types_1.ErrorType.InternalErrorType.AuthMissing
    )
  }
  return config
}
function requestFailed(error) {
  const { response } = error
  const err = new types_1.AppError(
    response === null || response === void 0 ? void 0 : response.data.message,
    response === null || response === void 0 ? void 0 : response.data.type
  )
  err.setStatusCode(response === null || response === void 0 ? void 0 : response.status)
  err.setErrorActions(response === null || response === void 0 ? void 0 : response.data.actions)
  throw err
}
exports.default = config => {
  const { baseURL, apiKey } = config
  const instance = axios_1.default.create({
    baseURL,
    headers: {
      [constants_1.API_KEY_NAME]: apiKey
    }
  })
  instance.interceptors.request.use(checkApiKey)
  instance.interceptors.response.use(undefined, requestFailed)
  return instance
}
//# sourceMappingURL=axios.js.map
