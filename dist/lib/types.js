'use strict'
var _a, _b, _c, _d, _e
Object.defineProperty(exports, '__esModule', { value: true })
exports.Domain = exports.DTO = exports.ConversationBase = exports.ConversationType = exports.MessageBase = exports.ReactionsObject = exports.ReactionType = exports.MessageType = exports.SystemMessageBase = exports.UserSimpleInfo = exports.SystemMessageType = exports.DocumentBase = exports.AudioBase = exports.VideoBase = exports.ImageBase = exports.MediaBase = exports.ConversationUsersBatchBase = exports.ImageInfoMedia = exports.SubImageMinimal = exports.ImageSizes = exports.FileType = exports.AuthBase = exports.DefaultGroupProfileImageBase = exports.AppError = exports.ErrorType = exports.HttpMethod = void 0
var HttpMethod
;(function(HttpMethod) {
  HttpMethod['Post'] = 'post'
  HttpMethod['Get'] = 'get'
  HttpMethod['Delete'] = 'delete'
  HttpMethod['Patch'] = 'patch'
  HttpMethod['Put'] = 'put'
})((HttpMethod = exports.HttpMethod || (exports.HttpMethod = {})))
var ErrorType
;(function(ErrorType) {
  let InternalErrorType
  ;(function(InternalErrorType) {
    InternalErrorType['Unknown'] = 'InternalUnknown'
    InternalErrorType['Uninitialized'] = 'InternalUninitialized'
    InternalErrorType['AuthMissing'] = 'InternalAuthMissing'
    InternalErrorType['BadData'] = 'InternalBadData'
    InternalErrorType['MissingParameter'] = 'InternalMissingParameter'
  })((InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {})))
  let AuthErrorType
  ;(function(AuthErrorType) {
    AuthErrorType['InvalidKeyErrorType'] = 'AuthErrorTypeInvalidKey'
    AuthErrorType['InvalidUserType'] = 'AuthErrorTypeInvalidUserType'
    AuthErrorType['NotFoundErrorType'] = 'AuthErrorTypeNotFound'
    AuthErrorType['MissingParamsErrorType'] = 'AuthErrorTypeMissingParams'
    AuthErrorType['InvalidPassword'] = 'AuthErrorTypeInvalidPassword'
    AuthErrorType['TokenExpiredType'] = 'AuthErrorTypeTokenExpired'
    AuthErrorType['AppHasNoNotificationSetup'] = 'AuthErrorTypeAppHasNoNotificationSetup'
    AuthErrorType['NotPartOfApp'] = 'AuthErrorTypeNotPartOfApp'
    AuthErrorType['InvalidWebhookKey'] = 'AuthErrorTypeInvalidWebhookKey'
    AuthErrorType['NoAccessToUser'] = 'AuthErrorTypeNoAccessToUser'
    AuthErrorType['ServerNoAccessToUser'] = 'AuthErrorTypeServerNoAccessToUser'
    AuthErrorType['IncorrectPassword'] = 'AuthErrorTypeIncorrectPassword'
    AuthErrorType['NoSamePassword'] = 'AuthErrorTypeNoSamePassword'
    AuthErrorType['NoAppAccessToUser'] = 'AuthErrorTypeNoAppAccessToUser'
    AuthErrorType['TokenNotStarted'] = 'AuthErrorTypeTokenNotStarted'
    AuthErrorType['AccountAlreadyExists'] = 'AuthErrorTypeAccountAlreadyExists'
    AuthErrorType['UserNotFound'] = 'AuthErrorTypeUserNotFound'
    AuthErrorType['InvalidToken'] = 'AuthErrorTypeInvalidToken'
    AuthErrorType['MissingAuthHeaderField'] = 'AuthErrorTypeMissingAuthHeaderField'
    AuthErrorType['InvalidCredentials'] = 'AuthErrorTypeInvalidCredentials'
    AuthErrorType['HashGeneration'] = 'AuthErrorTypeHashGeneration'
    AuthErrorType['HashComparison'] = 'AuthErrorTypeHashComparison'
    AuthErrorType['RefreshTokenAlreadyExchanged'] = 'AuthErrorTypeRefreshTokenAlreadyExchanged'
    AuthErrorType['RefreshTokenNotFound'] = 'AuthErrorTypeRefreshTokenNotFound'
    AuthErrorType['UserDisabled'] = 'AuthErrorTypeUserDisabled'
  })((AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {})))
  let FileErrorType
  ;(function(FileErrorType) {
    FileErrorType['SizeLimitExceeded'] = 'FileErrorTypeSizeLimitExceeded'
    FileErrorType['NoFileSentType'] = 'FileErrorTypeNoFileSentType'
    FileErrorType['InvalidFieldNameForFile'] = 'FileErrorTypeInvalidFieldNameForFile'
    FileErrorType['InvalidMetadata'] = 'FileErrorTypeInvalidMetadata'
    FileErrorType['FileSizeLimitExceeded'] = 'FileErrorTypeFileSizeLimitExceeded'
    FileErrorType['InvalidFileName'] = 'FileErrorTypeInvalidFileName'
    FileErrorType['NotSent'] = 'FileErrorTypeNotSent'
    FileErrorType['NotFound'] = 'FileErrorTypeNotFound'
    FileErrorType['NoThumbnailFound'] = 'FileErrorTypeNoThumbnailFound'
    FileErrorType['ImageNotForConversation'] = 'FileErrorTypeImageNotForConversation'
    FileErrorType['ImageNotForUser'] = 'FileErrorTypeImageNotForUser'
    FileErrorType['FileNotForMessage'] = 'FileErrorTypeFileNotForMessage'
    FileErrorType['SizeNotFound'] = 'FileErrorTypeSizeNotFound'
    FileErrorType['NotForApp'] = 'FileErrorTypeNotForApp'
    FileErrorType['IncorrectFileType'] = 'FileErrorTypeIncorrectFileType'
  })((FileErrorType = ErrorType.FileErrorType || (ErrorType.FileErrorType = {})))
  let ServerErrorType
  ;(function(ServerErrorType) {
    ServerErrorType['UnknownError'] = 'UnknownError'
    ServerErrorType['InternalServerError'] = 'InternalServerError'
  })((ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {})))
  let GeneralErrorType
  ;(function(GeneralErrorType) {
    GeneralErrorType['MissingQueryParams'] = 'GeneralErrorTypeMissingQueryParams'
    GeneralErrorType['RouteNotFound'] = 'GeneralErrorTypeRouteNotFound'
    GeneralErrorType['LimitNotValid'] = 'GeneralErrorTypeLimitNotValid'
    GeneralErrorType['OffsetNotValid'] = 'GeneralErrorTypeOffsetNotValid'
  })((GeneralErrorType = ErrorType.GeneralErrorType || (ErrorType.GeneralErrorType = {})))
  let MessageErrorType
  ;(function(MessageErrorType) {
    MessageErrorType['InvalidDate'] = 'MessageErrorTypeInvalidDate'
    MessageErrorType['NotFound'] = 'MessageErrorTypeNotFound'
    MessageErrorType['NotFoundForUser'] = 'MessageErrorTypeNotFoundForUser'
    MessageErrorType['MessageAlreadyRead'] = 'MessageErrorTypeMessageAlreadyRead'
    MessageErrorType['CannotDeleteSystemMessage'] = 'MessageErrorTypeCannotDeleteSystemMessage'
  })((MessageErrorType = ErrorType.MessageErrorType || (ErrorType.MessageErrorType = {})))
  let DateErrorType
  ;(function(DateErrorType) {
    DateErrorType['InvalidFromDateError'] = 'DateErrorTypeInvalidFromDateError'
    DateErrorType['InvalidToDateError'] = 'DateErrorTypeInvalidToDateError'
  })((DateErrorType = ErrorType.DateErrorType || (ErrorType.DateErrorType = {})))
  let UserErrorType
  ;(function(UserErrorType) {
    UserErrorType['AlreadyMuted'] = 'UserErrorTypeAlreadyMuted'
    UserErrorType['NotOnMute'] = 'UserErrorTypeNotOnMute'
    UserErrorType['AlreadyEnabled'] = 'UserErrorTypeAlreadyEnabled'
    UserErrorType['AlreadyDisabled'] = 'UserErrorTypeAlreadyDisabled'
  })((UserErrorType = ErrorType.UserErrorType || (ErrorType.UserErrorType = {})))
  let ConversationErrorType
  ;(function(ConversationErrorType) {
    ConversationErrorType['NotFound'] = 'ConversationErrorTypeNotFound'
    ConversationErrorType['CannotConverseWithUser'] = 'ConversationErrorTypeCannotConverseWithUser'
    ConversationErrorType['NotAnOwner'] = 'ConversationErrorTypeNotAnOwner'
    ConversationErrorType['AlreadyAnOwner'] = 'ConversationErrorTypeAlreadyAnOwner'
    ConversationErrorType['NotFoundForApp'] = 'ConversationErrorTypeNotFoundForApp'
    ConversationErrorType['InvalidType'] = 'ConversationErrorTypeInvalidType'
    ConversationErrorType['InvalidConversationUserType'] =
      'ConversationErrorTypeInvalidConversationUserType'
    ConversationErrorType['MustBeOwner'] = 'ConversationErrorTypeMustBeOwner'
    ConversationErrorType['NotPartOfApp'] = 'ConversationErrorTypeNotPartOfApp'
    ConversationErrorType['NotFoundForUser'] = 'ConversationErrorTypeNotFoundForUser'
    ConversationErrorType['AlreadyExists'] = 'ConversationErrorTypeAlreadyExists'
    ConversationErrorType['CannotChatWithSelf'] = 'ConversationErrorTypeCannotChatWithSelf'
    ConversationErrorType['AlreadyMuted'] = 'ConversationErrorTypeAlreadyMuted'
    ConversationErrorType['NotOnMute'] = 'ConversationErrorTypeNotOnMute'
    ConversationErrorType['AlreadyPinned'] = 'ConversationErrorTypeAlreadyPinned'
    ConversationErrorType['NotPinned'] = 'ConversationErrorTypeNotPinned'
    ConversationErrorType['UsersNotInApp'] = 'ConversationErrorTypeUsersNotInApp'
    ConversationErrorType['NoValidUsersToAdd'] = 'ConversationErrorTypeNoValidUsersToAdd'
    ConversationErrorType['NoValidUsersToRemove'] = 'ConversationErrorTypeNoValidUsersToRemove'
    ConversationErrorType['CannotChangeAccessToSelf'] =
      'ConversationErrorTypeCannotChangeAccessToSelf'
    ConversationErrorType['CannotAddExistingUsers'] = 'ConversationErrorTypeCannotAddExistingUsers'
    ConversationErrorType['AlreadyRemovedUsers'] = 'ConversationErrorTypeAlreadyRemovedUsers'
    ConversationErrorType['UsersNotInConversation'] = 'ConversationErrorTypeUsersNotInConversation'
    ConversationErrorType['CannotRemoveOwners'] = 'ConversationErrorTypeCannotRemoveOwners'
  })(
    (ConversationErrorType =
      ErrorType.ConversationErrorType || (ErrorType.ConversationErrorType = {}))
  )
})((ErrorType = exports.ErrorType || (exports.ErrorType = {})))
class AppError {
  constructor(err, type) {
    this.type = ErrorType.InternalErrorType.Unknown
    this.setErrorActions = params => {
      const { authFailed, exchange } = params
      this.exchange = exchange
      this.authFailed = authFailed
    }
    this.setStatusCode = statusCode => {
      this.statusCode = statusCode
    }
    this.setMessage = msg => {
      this.message = msg
    }
    if (type) {
      this.type = type
    }
    if (err instanceof Error) {
      this.error = err
      this.message = err.message
    } else {
      this.message = err
    }
  }
}
exports.AppError = AppError
class DefaultGroupProfileImageBase {}
exports.DefaultGroupProfileImageBase = DefaultGroupProfileImageBase
class AuthBase {}
exports.AuthBase = AuthBase
class UserBase {}
class UserDetail extends UserBase {}
var FileType
;(function(FileType) {
  FileType['Video'] = 'video'
  FileType['Image'] = 'image'
  FileType['Audio'] = 'audio'
  FileType['Other'] = 'other'
})((FileType = exports.FileType || (exports.FileType = {})))
var ImageSizes
;(function(ImageSizes) {
  ImageSizes['Tiny'] = 'tiny'
  ImageSizes['Small'] = 'small'
  ImageSizes['Medium'] = 'medium'
  ImageSizes['Large'] = 'large'
  ImageSizes['XLarge'] = 'xlarge'
})((ImageSizes = exports.ImageSizes || (exports.ImageSizes = {})))
class SubImageMinimal {}
exports.SubImageMinimal = SubImageMinimal
class ImageInfoMedia {}
exports.ImageInfoMedia = ImageInfoMedia
class Paginatable {}
class ConversationConversableUsersBatchMeta extends Paginatable {}
class ConversationUsersBatchMeta extends Paginatable {}
var ConversationUserQueryType
;(function(ConversationUserQueryType) {
  ConversationUserQueryType['All'] = 'all'
  ConversationUserQueryType['Current'] = 'current'
  ConversationUserQueryType['Removed'] = 'removed'
})(ConversationUserQueryType || (ConversationUserQueryType = {}))
class ConversationUsersBatchBase extends ConversationUsersBatchMeta {}
exports.ConversationUsersBatchBase = ConversationUsersBatchBase
class MediaBase {}
exports.MediaBase = MediaBase
class ImageBase extends MediaBase {
  constructor() {
    super(...arguments)
    this.fileType = FileType.Image
    this.videoInfo = null
  }
}
exports.ImageBase = ImageBase
class VideoBase extends MediaBase {
  constructor() {
    super(...arguments)
    this.fileType = FileType.Video
    this.imageInfo = null
  }
}
exports.VideoBase = VideoBase
class AudioBase extends MediaBase {
  constructor() {
    super(...arguments)
    this.fileType = FileType.Audio
    this.imageInfo = null
  }
}
exports.AudioBase = AudioBase
class DocumentBase extends MediaBase {
  constructor() {
    super(...arguments)
    this.fileType = FileType.Other
    this.imageInfo = null
    this.videoInfo = null
  }
}
exports.DocumentBase = DocumentBase
var SystemMessageType
;(function(SystemMessageType) {
  SystemMessageType['MemberRemoved'] = 'MemberRemoved'
  SystemMessageType['MemberAdded'] = 'MemberAdded'
  SystemMessageType['OwnerRemoved'] = 'OwnerRemoved'
  SystemMessageType['OwnerAdded'] = 'OwnerAdded'
  SystemMessageType['GroupPictureChanged'] = 'GroupPictureChanged'
  SystemMessageType['GroupNameChanged'] = 'GroupNameChanged'
  SystemMessageType['GroupDisabled'] = 'GroupDisabled'
  SystemMessageType['GroupCreated'] = 'GroupCreated'
  SystemMessageType['IndividualChatCreated'] = 'IndividualChatCreated'
})((SystemMessageType = exports.SystemMessageType || (exports.SystemMessageType = {})))
class UserSimpleInfo {}
exports.UserSimpleInfo = UserSimpleInfo
class SystemMessageBase {}
exports.SystemMessageBase = SystemMessageBase
var MessageType
;(function(MessageType) {
  MessageType['System'] = 'system'
  MessageType['Text'] = 'text'
  MessageType['Image'] = 'image'
  MessageType['Video'] = 'video'
  MessageType['Document'] = 'document'
  MessageType['Gif'] = 'gif'
  MessageType['Emoji'] = 'emoji'
  MessageType['Audio'] = 'audio'
})((MessageType = exports.MessageType || (exports.MessageType = {})))
var ReactionType
;(function(ReactionType) {
  ReactionType['Thumb'] = 'Thumb'
  ReactionType['Heart'] = 'Heart'
  ReactionType['Clap'] = 'Clap'
  ReactionType['HaHa'] = 'HaHa'
  ReactionType['Exclamation'] = 'Exclamation'
})((ReactionType = exports.ReactionType || (exports.ReactionType = {})))
class ReactionsObject {
  constructor() {
    this[_a] = 0
    this[_b] = 0
    this[_c] = 0
    this[_d] = 0
    this[_e] = 0
  }
}
exports.ReactionsObject = ReactionsObject
;(_a = ReactionType.Thumb),
  (_b = ReactionType.Heart),
  (_c = ReactionType.Clap),
  (_d = ReactionType.HaHa),
  (_e = ReactionType.Exclamation)
class MessageBase {}
exports.MessageBase = MessageBase
var ConversationType
;(function(ConversationType) {
  ConversationType['Group'] = 'group'
  ConversationType['Individual'] = 'individual'
})((ConversationType = exports.ConversationType || (exports.ConversationType = {})))
class ConversationBase {}
exports.ConversationBase = ConversationBase
var DTO
;(function(DTO) {
  class DefaultGroupProfileImage extends DefaultGroupProfileImageBase {}
  DTO.DefaultGroupProfileImage = DefaultGroupProfileImage
  class Auth extends AuthBase {}
  DTO.Auth = Auth
  class User extends UserDetail {}
  DTO.User = User
  class UserSelf extends User {}
  DTO.UserSelf = UserSelf
  class ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {}
  DTO.ConversationConversableUsersBatch = ConversationConversableUsersBatch
  class ConversationUser extends User {}
  DTO.ConversationUser = ConversationUser
  class ConversationUsersBatch extends ConversationUsersBatchBase {}
  DTO.ConversationUsersBatch = ConversationUsersBatch
  class SystemMessage extends SystemMessageBase {}
  DTO.SystemMessage = SystemMessage
  class Message extends MessageBase {}
  DTO.Message = Message
  class ConversationInfo {}
  DTO.ConversationInfo = ConversationInfo
  class GroupConversationInfo extends ConversationInfo {}
  DTO.GroupConversationInfo = GroupConversationInfo
  class UserConversationInfo extends ConversationInfo {}
  DTO.UserConversationInfo = UserConversationInfo
  class Conversation extends ConversationBase {}
  DTO.Conversation = Conversation
  class GroupConversation extends Conversation {}
  DTO.GroupConversation = GroupConversation
  class IndividualConversation extends Conversation {}
  DTO.IndividualConversation = IndividualConversation
  class MessageDeleted {}
  DTO.MessageDeleted = MessageDeleted
  class ConversationUsersBasic {}
  DTO.ConversationUsersBasic = ConversationUsersBasic
})((DTO = exports.DTO || (exports.DTO = {})))
var Domain
;(function(Domain) {
  class DefaultGroupProfileImage extends DefaultGroupProfileImageBase {}
  Domain.DefaultGroupProfileImage = DefaultGroupProfileImage
  class Auth extends AuthBase {}
  Domain.Auth = Auth
  class User extends UserDetail {}
  Domain.User = User
  class UserSelf extends User {}
  Domain.UserSelf = UserSelf
  class ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {}
  Domain.ConversationConversableUsersBatch = ConversationConversableUsersBatch
  class ConversationUser extends User {}
  Domain.ConversationUser = ConversationUser
  class ConversationUsersBatch extends ConversationUsersBatchBase {}
  Domain.ConversationUsersBatch = ConversationUsersBatch
  class SystemMessage extends SystemMessageBase {}
  Domain.SystemMessage = SystemMessage
  class Message extends MessageBase {}
  Domain.Message = Message
  class ConversationInfo {}
  Domain.ConversationInfo = ConversationInfo
  class GroupConversationInfo extends ConversationInfo {}
  Domain.GroupConversationInfo = GroupConversationInfo
  class UserConversationInfo extends ConversationInfo {}
  Domain.UserConversationInfo = UserConversationInfo
  class Conversation extends ConversationBase {}
  Domain.Conversation = Conversation
  class GroupConversation extends Conversation {}
  Domain.GroupConversation = GroupConversation
  class IndividualConversation extends Conversation {}
  Domain.IndividualConversation = IndividualConversation
  class MessageDeleted {}
  Domain.MessageDeleted = MessageDeleted
  class ConversationUsersBasic {}
  Domain.ConversationUsersBasic = ConversationUsersBasic
})((Domain = exports.Domain || (exports.Domain = {})))
//# sourceMappingURL=types.js.map
