export declare enum HttpMethod {
  Post = 'post',
  Get = 'get',
  Delete = 'delete',
  Patch = 'patch',
  Put = 'put'
}
export declare namespace ErrorType {
  enum InternalErrorType {
    Unknown = 'InternalUnknown',
    Uninitialized = 'InternalUninitialized',
    AuthMissing = 'InternalAuthMissing',
    BadData = 'InternalBadData',
    MissingParameter = 'InternalMissingParameter'
  }
  type Type =
    | InternalErrorType
    | AuthErrorType
    | FileErrorType
    | ServerErrorType
    | GeneralErrorType
    | MessageErrorType
    | UserErrorType
    | ConversationErrorType
  enum AuthErrorType {
    InvalidKeyErrorType = 'AuthErrorTypeInvalidKey',
    InvalidUserType = 'AuthErrorTypeInvalidUserType',
    NotFoundErrorType = 'AuthErrorTypeNotFound',
    MissingParamsErrorType = 'AuthErrorTypeMissingParams',
    InvalidPassword = 'AuthErrorTypeInvalidPassword',
    TokenExpiredType = 'AuthErrorTypeTokenExpired',
    AppHasNoNotificationSetup = 'AuthErrorTypeAppHasNoNotificationSetup',
    NotPartOfApp = 'AuthErrorTypeNotPartOfApp',
    InvalidWebhookKey = 'AuthErrorTypeInvalidWebhookKey',
    NoAccessToUser = 'AuthErrorTypeNoAccessToUser',
    ServerNoAccessToUser = 'AuthErrorTypeServerNoAccessToUser',
    IncorrectPassword = 'AuthErrorTypeIncorrectPassword',
    NoSamePassword = 'AuthErrorTypeNoSamePassword',
    NoAppAccessToUser = 'AuthErrorTypeNoAppAccessToUser',
    TokenNotStarted = 'AuthErrorTypeTokenNotStarted',
    AccountAlreadyExists = 'AuthErrorTypeAccountAlreadyExists',
    UserNotFound = 'AuthErrorTypeUserNotFound',
    InvalidToken = 'AuthErrorTypeInvalidToken',
    MissingAuthHeaderField = 'AuthErrorTypeMissingAuthHeaderField',
    InvalidCredentials = 'AuthErrorTypeInvalidCredentials',
    HashGeneration = 'AuthErrorTypeHashGeneration',
    HashComparison = 'AuthErrorTypeHashComparison',
    RefreshTokenAlreadyExchanged = 'AuthErrorTypeRefreshTokenAlreadyExchanged',
    RefreshTokenNotFound = 'AuthErrorTypeRefreshTokenNotFound',
    UserDisabled = 'AuthErrorTypeUserDisabled'
  }
  enum FileErrorType {
    SizeLimitExceeded = 'FileErrorTypeSizeLimitExceeded',
    NoFileSentType = 'FileErrorTypeNoFileSentType',
    InvalidFieldNameForFile = 'FileErrorTypeInvalidFieldNameForFile',
    InvalidMetadata = 'FileErrorTypeInvalidMetadata',
    FileSizeLimitExceeded = 'FileErrorTypeFileSizeLimitExceeded',
    InvalidFileName = 'FileErrorTypeInvalidFileName',
    NotSent = 'FileErrorTypeNotSent',
    NotFound = 'FileErrorTypeNotFound',
    NoThumbnailFound = 'FileErrorTypeNoThumbnailFound',
    ImageNotForConversation = 'FileErrorTypeImageNotForConversation',
    ImageNotForUser = 'FileErrorTypeImageNotForUser',
    FileNotForMessage = 'FileErrorTypeFileNotForMessage',
    SizeNotFound = 'FileErrorTypeSizeNotFound',
    NotForApp = 'FileErrorTypeNotForApp',
    IncorrectFileType = 'FileErrorTypeIncorrectFileType'
  }
  enum ServerErrorType {
    UnknownError = 'UnknownError',
    InternalServerError = 'InternalServerError'
  }
  enum GeneralErrorType {
    MissingQueryParams = 'GeneralErrorTypeMissingQueryParams',
    RouteNotFound = 'GeneralErrorTypeRouteNotFound',
    LimitNotValid = 'GeneralErrorTypeLimitNotValid',
    OffsetNotValid = 'GeneralErrorTypeOffsetNotValid'
  }
  enum MessageErrorType {
    InvalidDate = 'MessageErrorTypeInvalidDate',
    NotFound = 'MessageErrorTypeNotFound',
    NotFoundForUser = 'MessageErrorTypeNotFoundForUser',
    MessageAlreadyRead = 'MessageErrorTypeMessageAlreadyRead',
    CannotDeleteSystemMessage = 'MessageErrorTypeCannotDeleteSystemMessage'
  }
  enum DateErrorType {
    InvalidFromDateError = 'DateErrorTypeInvalidFromDateError',
    InvalidToDateError = 'DateErrorTypeInvalidToDateError'
  }
  enum UserErrorType {
    AlreadyMuted = 'UserErrorTypeAlreadyMuted',
    NotOnMute = 'UserErrorTypeNotOnMute',
    AlreadyEnabled = 'UserErrorTypeAlreadyEnabled',
    AlreadyDisabled = 'UserErrorTypeAlreadyDisabled'
  }
  enum ConversationErrorType {
    NotFound = 'ConversationErrorTypeNotFound',
    CannotConverseWithUser = 'ConversationErrorTypeCannotConverseWithUser',
    NotAnOwner = 'ConversationErrorTypeNotAnOwner',
    AlreadyAnOwner = 'ConversationErrorTypeAlreadyAnOwner',
    NotFoundForApp = 'ConversationErrorTypeNotFoundForApp',
    InvalidType = 'ConversationErrorTypeInvalidType',
    InvalidConversationUserType = 'ConversationErrorTypeInvalidConversationUserType',
    MustBeOwner = 'ConversationErrorTypeMustBeOwner',
    NotPartOfApp = 'ConversationErrorTypeNotPartOfApp',
    NotFoundForUser = 'ConversationErrorTypeNotFoundForUser',
    AlreadyExists = 'ConversationErrorTypeAlreadyExists',
    CannotChatWithSelf = 'ConversationErrorTypeCannotChatWithSelf',
    AlreadyMuted = 'ConversationErrorTypeAlreadyMuted',
    NotOnMute = 'ConversationErrorTypeNotOnMute',
    AlreadyPinned = 'ConversationErrorTypeAlreadyPinned',
    NotPinned = 'ConversationErrorTypeNotPinned',
    UsersNotInApp = 'ConversationErrorTypeUsersNotInApp',
    NoValidUsersToAdd = 'ConversationErrorTypeNoValidUsersToAdd',
    NoValidUsersToRemove = 'ConversationErrorTypeNoValidUsersToRemove',
    CannotChangeAccessToSelf = 'ConversationErrorTypeCannotChangeAccessToSelf',
    CannotAddExistingUsers = 'ConversationErrorTypeCannotAddExistingUsers',
    AlreadyRemovedUsers = 'ConversationErrorTypeAlreadyRemovedUsers',
    UsersNotInConversation = 'ConversationErrorTypeUsersNotInConversation',
    CannotRemoveOwners = 'ConversationErrorTypeCannotRemoveOwners'
  }
}
export declare class AppError {
  type: ErrorType.Type
  statusCode?: number
  error?: Error
  message?: string
  authFailed?: boolean
  exchange?: boolean
  setErrorActions: (params: { authFailed?: boolean; exchange?: boolean }) => void
  constructor(err?: string | Error, type?: ErrorType.Type)
  setStatusCode: (statusCode?: number) => void
  setMessage: (msg: string) => void
}
export declare class DefaultGroupProfileImageBase {
  filename: string
  original: string
  thumbnail: string
}
export declare class AuthBase {
  accessToken: string
  publisherToken: string
  refreshToken: string
}
declare class UserBase {
  id: string
  firstName: string
  lastName: string
  refId: string | null
}
declare class UserDetail extends UserBase {
  email: string
}
export declare enum FileType {
  Video = 'video',
  Image = 'image',
  Audio = 'audio',
  Other = 'other'
}
export declare enum ImageSizes {
  Tiny = 'tiny',
  Small = 'small',
  Medium = 'medium',
  Large = 'large',
  XLarge = 'xlarge'
}
export declare class SubImageMinimal {
  imageSize: ImageSizes
  width: number
  height: number
  postfix: string
}
export declare class ImageInfoMedia {
  width: number
  height: number
  subImagesInfo: SubImageMinimal[]
}
declare class Paginatable {
  search: string | null
  totalCount: number
  offset: number
  nextOffset: number
  limit: number
  completed: boolean
}
declare class ConversationConversableUsersBatchMeta extends Paginatable {
  includeRefIdInSearch: boolean
  conversationId: string
}
declare class ConversationUsersBatchMeta extends Paginatable {
  includeRefIdInSearch: boolean
  conversationId: string
}
declare enum ConversationUserQueryType {
  All = 'all',
  Current = 'current',
  Removed = 'removed'
}
export declare class ConversationUsersBatchBase extends ConversationUsersBatchMeta {
  type: ConversationUserQueryType
}
export declare class MediaBase {
  urlPath: string
  mimeType: string
  extension: string
  encoding: string
  filename: string
  fileSize: number
}
export declare class ImageBase extends MediaBase {
  fileType: FileType
  imageInfo: ImageInfoMedia
  videoInfo: null
}
export declare class VideoBase extends MediaBase {
  fileType: FileType
  imageInfo: null
  videoInfo: {
    duration: number
    thumbnail: ImageBase | null
  }
}
export declare class AudioBase extends MediaBase {
  fileType: FileType
  imageInfo: null
  videoInfo: {
    duration: number
    thumbnail: null
  }
}
export declare class DocumentBase extends MediaBase {
  fileType: FileType
  imageInfo: null
  videoInfo: null
}
export type FileBase = ImageBase | VideoBase | AudioBase | DocumentBase
export declare enum SystemMessageType {
  MemberRemoved = 'MemberRemoved',
  MemberAdded = 'MemberAdded',
  OwnerRemoved = 'OwnerRemoved',
  OwnerAdded = 'OwnerAdded',
  GroupPictureChanged = 'GroupPictureChanged',
  GroupNameChanged = 'GroupNameChanged',
  GroupDisabled = 'GroupDisabled',
  GroupCreated = 'GroupCreated',
  IndividualChatCreated = 'IndividualChatCreated'
}
export declare class UserSimpleInfo {
  firstName: string
  lastName: string
  id: string
  refId: string | null
}
export declare class SystemMessageBase {
  systemMessageType: SystemMessageType
  id: string
  refValue: string | null
  actorCount: number
  initiator: UserSimpleInfo | null
  actors: UserSimpleInfo[]
  text?: string | null
}
export declare enum MessageType {
  System = 'system',
  Text = 'text',
  Image = 'image',
  Video = 'video',
  Document = 'document',
  Gif = 'gif',
  Emoji = 'emoji',
  Audio = 'audio'
}
export declare enum ReactionType {
  Thumb = 'Thumb',
  Heart = 'Heart',
  Clap = 'Clap',
  HaHa = 'HaHa',
  Exclamation = 'Exclamation'
}
export declare class ReactionsObject {
  [ReactionType.Thumb]: number;
  [ReactionType.Heart]: number;
  [ReactionType.Clap]: number;
  [ReactionType.HaHa]: number;
  [ReactionType.Exclamation]: number
}
export declare class MessageBase {
  id: string
  conversationId: string
  type: MessageType
  text: string | null
  refUrl: string | null
  sequence: number
  reactions: ReactionsObject
}
export declare enum ConversationType {
  Group = 'group',
  Individual = 'individual'
}
export declare class ConversationBase {
  id: string
  lastReadMessageId: string | null
  type: ConversationType.Group | ConversationType.Individual
  isOwner: boolean
  visible: boolean
  serverCreated: boolean
  serverManaged: boolean
}
export declare namespace DTO {
  class DefaultGroupProfileImage extends DefaultGroupProfileImageBase {}
  class Auth extends AuthBase {}
  type Media = FileBase
  class User extends UserDetail {
    picture: Media | null
    disabled: boolean
    deletedAt: string | null
  }
  class UserSelf extends User {
    muteAllUntil: string | null
  }
  class ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {
    results: User[]
  }
  class ConversationUser extends User {
    isOwner: boolean
    deletedFromConversationAt: string | null
  }
  class ConversationUsersBatch extends ConversationUsersBatchBase {
    results: ConversationUser[]
  }
  class SystemMessage extends SystemMessageBase {}
  class Message extends MessageBase {
    createdAt: string
    sender: User | null
    file: Media | null
    sent: boolean
    systemMessage: SystemMessage | null
    deletedAt: string | null
  }
  class ConversationInfo {
    picture: Media | null
    name: string
  }
  class GroupConversationInfo extends ConversationInfo {}
  class UserConversationInfo extends ConversationInfo {
    id: string
    email: string
    refId: string | null
  }
  class Conversation extends ConversationBase {
    muteUntil: string | null
    pinnedAt: string | null
    badge: number | null
    startAfter: string | null
    endBefore: string | null
    deletedFromConversationAt: string | null
    createdAt: string
    updatedAt: string
    lastMessage: Message | null
    group?: ConversationInfo
    user?: UserConversationInfo
  }
  class GroupConversation extends Conversation {
    type: ConversationType.Group
    group: GroupConversationInfo
  }
  class IndividualConversation extends Conversation {
    type: ConversationType.Individual
    user: UserConversationInfo
  }
  type GenericConversation = GroupConversation | IndividualConversation
  class MessageDeleted {
    conversationId: string
    messageId: string
    deletedAt: string
  }
  class ConversationUsersBasic {
    isOwner: boolean
    id: string
    deletedFromConversationAt: string | null
    userDisabled: boolean
    userDeletedAt: string | null
  }
}
export declare namespace Domain {
  class DefaultGroupProfileImage extends DefaultGroupProfileImageBase {}
  class Auth extends AuthBase {}
  type Media = MediaBase
  class User extends UserDetail {
    picture: Media | null
    disabled: boolean
    deletedAt: Date | null
  }
  class UserSelf extends User {
    muteAllUntil: Date | null
  }
  class ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {
    results: User[]
  }
  class ConversationUser extends User {
    deletedFromConversationAt: Date | null
    isOwner: boolean
  }
  class ConversationUsersBatch extends ConversationUsersBatchBase {
    results: ConversationUser[]
  }
  class SystemMessage extends SystemMessageBase {}
  class Message extends MessageBase {
    createdAt: Date
    sender: User | null
    file: Media | null
    sent: boolean
    systemMessage: SystemMessage | null
    deletedAt: Date | null
  }
  class ConversationInfo {
    picture: Media | null
    name: string
  }
  class GroupConversationInfo extends ConversationInfo {}
  class UserConversationInfo extends ConversationInfo {
    id: string
    email: string
    refId: string | null
  }
  class Conversation extends ConversationBase {
    muteUntil: Date | null
    pinnedAt: Date | null
    badge: number | null
    startAfter: Date | null
    endBefore: Date | null
    deletedFromConversationAt: Date | null
    createdAt: Date
    updatedAt: Date
    lastMessage: Message | null
  }
  class GroupConversation extends Conversation {
    type: ConversationType.Group
    group: GroupConversationInfo
  }
  class IndividualConversation extends Conversation {
    type: ConversationType.Individual
    user: UserConversationInfo
  }
  type GenericConversation = GroupConversation | IndividualConversation
  class MessageDeleted {
    conversationId: string
    messageId: string
    deletedAt: Date
  }
  class ConversationUsersBasic {
    isOwner: boolean
    id: string
    deletedFromConversationAt: Date | null
    userDisabled: boolean
    userDeletedAt: Date | null
  }
}
export {}
